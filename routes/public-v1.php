<?php

use Illuminate\Support\Facades\Route;

Route::post('otp', 'AuthController@loginOrRegisterWithOtp')->name('otp');
Route::post('otp/verify', 'AuthController@otp')->name('otp.verify');
Route::post('login', 'AuthController@login')->name('login');
Route::get('/email/verify', 'VerificationController@verifyEmailVerification')->name('verify.email');

Route::prefix('/documentation/')->name('documentation.')->group(function () {
    Route::get('admin/{documentType?}', 'DocumentationController@showAdminApiDocumentation')->name('admin');
    Route::get('client/{documentType?}', 'DocumentationController@showClientApiDocumentation')->name('client');
});
