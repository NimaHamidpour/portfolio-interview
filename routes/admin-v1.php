<?php

Route::apiResource('users', 'UserController');
Route::apiResource('configs', 'ConfigController')->except(['show']);
Route::apiResource('logins', 'LoginController')->only(['index']);
Route::apiResource('categories', 'CategoryController');
Route::apiResource('tags', 'TagController');

Route::prefix('/verifications')->name('verification.')->group(function () {
    Route::get('/', 'VerificationController@index')->name('index');
    Route::patch('/{verification}/accept', 'VerificationController@acceptVerification')->name('accept');
    Route::patch('/{verification}/reject', 'VerificationController@rejectVerification')->name('reject');
});
