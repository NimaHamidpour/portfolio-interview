<?php

Route::apiResource('user', 'UserController')->only(['update']);
Route::apiResource('verification', 'VerificationController')->only(['store']);
Route::prefix('/tools')->name('tools.')->group(function () {
    Route::get('configs', 'ToolsController@getConfigs')->name('configs');
    Route::post('send-mail', 'ToolsController@sendMail')->name('send-mail');
});
Route::get('self', 'SelfController@userInfo')->name('self');
Route::apiResource('business', 'BusinessController');
