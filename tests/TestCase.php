<?php

namespace Tests;

use App\Enum\Currency as EnumCurrency;
use App\Models\User;
use App\Traits\ConvertDateTrait;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Tymon\JWTAuth\Facades\JWTAuth;


abstract class TestCase extends BaseTestCase
{
    use WithFaker;
    use RefreshDatabase;
    use CreatesApplication;
    use ConvertDateTrait;
    /**
     * @param User|null $loggedInUser Logged In User.
     *
     * @return User
     */
    public function actingAsUser(User $loggedInUser = null): User
    {
        if (empty($loggedInUser)) {
            $loggedInUser = User::factory()->create();
        }

        $this->actingAs($loggedInUser);

        return $loggedInUser;
    }

    /**
     * @param string $permission Permission.
     * @param User|null $loggedInUser
     * @return User
     */
    public function actingAsUserWithPermission(string $permission, User $loggedInUser = null): User
    {
        if (empty($loggedInUser)) {
            $loggedInUser = User::factory()->create();
        }
        Permission::create(['name' => $permission]);
        $loggedInUser->givePermissionTo($permission);
        $this->actingAs($loggedInUser);

        return $loggedInUser;
    }

    /**
     * @param string $role User Role.
     * @param User|null $loggedInUser
     * @return User
     */
    public function actingAsUserWithRole(string $role, User $loggedInUser = null): User
    {
        if (empty($loggedInUser)) {
            $loggedInUser = User::factory()->create();
        }
        Role::create(['name' => $role]);
        $loggedInUser->assignRole($role);

        $this->actingAs($loggedInUser);

        return $loggedInUser;
    }

    /**
     * @param Authenticatable $user
     * @param null $driver
     * @return $this|TestCase
     */
    public function actingAs(Authenticatable $user, $driver = null): TestCase|static
    {
        $token = JWTAuth::fromUser($user);
        $this->withHeader('Authorization', 'Bearer ' . $token);

        return $this;
    }

    /**
     * @param string $message
     * @param array $data
     * @return array
     */
    protected function getSuccessResponseStructure(string $message = '', array $data = []): array
    {
        return [
            'message' => $message,
            'data' => $data
        ];
    }

    /**
     * @param string $message
     * @param array $error
     * @return array
     */
    protected function getErrorResponseStructure(string $message = '', array $error = []): array
    {
        return [
            'message' => $message,
            'error' => $error
        ];
    }

    /**
     * @return \array[][]
     */
    protected function getValidationErrorResponseStructure(): array
    {
        return [
            'errors' => [
                'validations' => []
            ]
        ];
    }

    /**
     * @param string $routeUrl
     * @param array $inputData
     * @param string $fieldName
     * @param string|null $requestType
     */
    protected function checkSingleColumnValidationAsserts(
        string $routeUrl,
        array $inputData,
        string $fieldName,
        string|null $requestType = 'post'
    ): void
    {
        $response = $requestType == 'put' ?
            $this->putJson($routeUrl, $inputData) : $this->postJson($routeUrl, $inputData);
        $response
            ->assertJsonStructure(array_keys($this->getValidationErrorResponseStructure()), $this->getValidationErrorResponseStructure())
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertTrue($response->original["errors"]["validations"][0]['fieldName'] === $fieldName);
        $this->assertDatabaseCount($this->getTableName(), 0);
    }

    /**
     * @param string $routeName
     * @param int|string|array|null $resourceParam
     * @return string
     */
    protected function makeResourceRouteUrl(string $routeName, int|string|array $resourceParam = null): string
    {
        return empty($resourceParam) ?
            route(sprintf('%s.%s', $this->resourceBaseRouteName, $routeName)) :
            route(sprintf('%s.%s', $this->resourceBaseRouteName, $routeName), $resourceParam);
    }

    /**
     * @return string
     */
    public function fromCreatedAt(): string
    {
        $now = Carbon::now();
        return $this->jalaliDate($now,'-');
    }

    /**
     * @return string
     */
    public function toCreatedAt(): string
    {
        $tomorrow = Carbon::now()->addDays(1);
        return $this->jalaliDate($tomorrow,'-');
    }
}
