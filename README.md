
## Project Angel

### Angel commands

- "**make build start**" for the first time after cloning the project
- "**make up**" to run entire containers
- "**make shell**" to execute to php container
- "**make destroy**" to stop and remove all containers
- "**make status**" to get status of all running containers.
- "**make db**" to connecting to your database

### Angel in development mode
- composer dump-autoload
- composer install
- php artisan optimize:clear
- php artisan migrate
- php artisan db:seed
- php artisan storage:link
- php artisan serve
