<?php

use App\Enum\Role;

return [
    [
        'username' => 'angel',
        'mobile' => '09123456789',
        'password' => 'secret',
        'role' => Role::SUPER_ADMIN,
        'email' => 'superadmin@angel.test',
        'register_ip' => '192.168.1.1',
        'first_name' => 'superadmin',
        'last_name' => 'angel',
    ]
    ,[
        'username' => 'admin',
        'first_name' => 'admin',
        'last_name' => 'angel',
        'mobile' => '09124109855',
        'email' => 'admin@angel.test',
        'password' => 'secret',
        'register_ip' => '192.168.1.1',
        'role' => Role::ADMIN,
    ],
    [
        'username' => 'operator',
        'first_name' => 'operator',
        'last_name' => 'angel',
        'mobile' => '09355807196',
        'email' => 'operator@angel.test',
        'password' => 'secret',
        'register_ip' => '192.168.1.1',
        'role' => Role::OPERATOR,
    ],
    [
        'username' => 'user',
        'first_name' => 'user',
        'last_name' => 'angel',
        'mobile' => '09133390765',
        'email' => 'user@angel.test',
        'password' => 'secret',
        'register_ip' => '192.168.1.1',
        'role' => Role::USER,
    ]
];
