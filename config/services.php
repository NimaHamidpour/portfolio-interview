<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Lib
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'endpoint' => env('MAILGUN_ENDPOINT', 'https://api.mailgun.net/'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'verification' => [
        'email_verification_verify_url' => sprintf('%s/%s', env('APP_URL'), 'api/v1/email/verify'),
        'email_verification_expiration_timestamp' => 12, // as Hour
        'email_verification_deeplink' => env('EMAIL_VERIFICAIONT_DEEPLINK', 'odin://verification/email/verify')
    ],
];
