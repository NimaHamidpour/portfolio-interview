<?php

/*
|--------------------------------------------------------------------------
| Authentication Language Lines
|--------------------------------------------------------------------------
|
| The following language lines are used during authentication for various
| messages.php that we need to display to the user. You are free to modify
| these language lines according to your application's requirements.
|
*/

return [
    'failed'   => 'مشخصات وارد شده با اطلاعات ما سازگار نیست.',
    'password' => 'رمز عبور شما معتبر نیست.',
    'throttle' => 'دفعات تلاش شما برای ورود بیش از حد مجاز است. لطفا پس از :seconds ثانیه مجددا تلاش فرمایید.',
    'otp_ttl' => 'لطفا پس از %s ثانیه مجددا تلاش کنید.',
    'otp_is_not_correct' => 'رمز یکبار مصرف صحیح نمی‌باشد',
    'otp_not_set' => 'مجددا برای رمز یکبار مصرف اقدام کنید.',
    'count_of_tries' => 'تعداد دفعات تلاش شما بیش از حد مجاز است.',
    'otp_send_success' => 'رمز یکبار مصرف برای شما با موفقیت ارسال شد.',
    'otp_succeeded' => 'رمز یکبار مصرف صحیح است.',
    'mobile_or_password_incorrect' => 'شماره موبایل با رمز عبور مطابقت ندارد.',
    'login_succeeded' => 'ورود موفق',
];
