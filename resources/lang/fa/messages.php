<?php
return [
    'response' => [
        'ok' => 'انجام فرآیند موفق بود',
        'created' => 'منبع جدید ایجاد شد',
        'bad_request' => 'بروز خطا در انجام کار',
        'unauthorized' => 'توکن نامعتبر',
        'forbidden' => 'دسترسی نامعتبر',
        'not_found' => 'منبع مورد نظر پیدا نشد',
        'not_acceptable' => 'مجوز استفاده نامعتبر',
        'conflict' => 'مغایرت اطلاعات',
        'updated' => 'با موفقیت به روزرسانی شد',
    ],
    'verification' => [
        'pending' => 'در انتظار تایید',
        'accepted' => 'تایید شده',
        'rejected' => 'رد شده',
        'USER_INFO' => 'اطلاعات شخصی',
        'LEGAL_INFO' => 'اطلاعات شرکت',
        'ID_CARD_FRONT' => 'تصویر روی کارت ملی',
        'ID_CARD_BACK' => 'تصویر پشت کارت ملی',
        'CUSTOM_ID_FIRST' => 'تصویر کارت دلخواه اول',
        'CUSTOM_ID_SECOND' => 'تصویر کارت دلخواه دوم',
        'USER_SELFIE' => 'تصویر احراز هویت',
        'PHONE' => 'تلفن ثابت',
        'BANK_CARD' => 'کارت بانکی',
        'EMAIL' => 'ایمیل',
        'VIDEO' => 'ویدئو احراز هویت',
        'verification_already_exists' => 'امکان ثبت اطلاعات مقدور نیست. وضعیت کاربر در این مرحله :value می باشد.',
        'token_is_not_valid' => 'کد تایید ارسال شده معتبر نمی باشد.',
        'token_is_expired' => 'کد تایید ارسال شده منتقضی شده است.',
        'token_verification_not_found' => 'احراز هویتی با این کد پیدا نشد. لطفا مجددا سعی نمایید.',
        'verification_status_is_not_valid' => 'مدارک تایید شده قابل تغیبر نمی باشد.',
    ],
    'currencies' => [
        'currency_has_gateways_delete_fail' => 'خطا! لطفا ابتدا وضعیت درگاه هایی که از این ارز استفاده می کنند را تغییر دهید.',
    ],
    'gateways' => [
        'gateway_has_services_delete_fail' => 'خطا! لطفا ابتدا وضعیت سرویس هایی که از این درگاه استفاده می کنند را تغییر دهید.',
    ]
];
