<!doctype html> <!-- Important: must specify -->
<html>
<head>
    <meta charset="utf-8"> <!-- Important: rapi-doc uses utf8 characters -->
    <script type="module" src="{{ asset('apidoc-assets/rapidoc/rapidoc-min.js') }}"></script>
</head>
<body>
<rapi-doc
    spec-url="{{ $documentPath }}"
    theme = "dark"
> </rapi-doc>
</body>
</html>
