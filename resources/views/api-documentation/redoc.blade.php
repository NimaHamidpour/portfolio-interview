<!DOCTYPE html>
<html>
<head>
    <title>Redoc</title>
    <!-- needed for adaptive design -->
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--
    Redoc doesn't change outer page styles
    -->
    <style>
        body {
            margin: 0;
            padding: 0;
        }
    </style>
</head>
<body>
<redoc spec-url="{{ $documentPath }}"></redoc>
<script src="{{ asset('apidoc-assets/redoc/redoc.standalone.js') }}"> </script>
</body>
</html>
