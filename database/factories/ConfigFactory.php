<?php

namespace Database\Factories;

use App\Enum\Config;
use App\Models\Config as ConfigModel;
use Illuminate\Database\Eloquent\Factories\Factory;

class ConfigFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ConfigModel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            Config::KEY =>  $this->faker->word . rand(0,9999),
            Config::VALUE => $this->faker->word,
            Config::GROUP => Config::$configGroups[0]
        ];
    }
}
