<?php

namespace Database\Factories;

use App\Enum\User;
use App\Models\User as UserModel;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserModel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            User::AVATAR => $this->faker->imageUrl,
            User::USERNAME => $this->faker->userName,
            User::FIRST_NAME => $this->faker->firstName,
            User::LAST_NAME => $this->faker->lastName,
            User::MOBILE => '09' . $this->faker->numerify('#########'),
            User::NATIONAL_CODE => $this->faker->numerify('##########'),
            User::EMAIL => $this->faker->email,
            User::ZIPCODE => $this->faker->numerify('##########'),
            User::ADDRESS => $this->faker->address,
            User::BIRTH_DATE => $this->faker->dateTimeBetween('1970-01-01', '2020-01-01')->format('Y-m-d'),
            User::PHONE => (string)rand(10000000000, 999999999999),
            User::REGISTER_IP => $this->faker->ipv4,
            User::STATUS =>rand(0,1),
            User::ALLOW_CHARGE => rand(0,1),
            User::LOCK_BALANCE => rand(0,1),
            User::LOCK_WITHDRAW => rand(0,1),
            User::PASSWORD => Hash::make(123),
            User::AFFILIATE_CODE =>rand(10000,99999)
        ];
    }

    public function customValue($user)
    {
        return $this->state(function (array $attribute) use ($user) {
            return [
                User::USERNAME    =>  $user['username'],
                User::MOBILE      =>  $user['mobile'],
                User::PASSWORD    =>  Hash::make($user['password']),
                User::EMAIL       =>  $user['email'],
                User::REGISTER_IP =>  $user['register_ip'],
                User::PHONE       =>  $user['phone'],
                User::FIRST_NAME  =>  $user['first_name'],
                User::LAST_NAME   =>  $user['last_name']
            ] ;
        });
    }

}
