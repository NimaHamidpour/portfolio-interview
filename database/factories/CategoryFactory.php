<?php

namespace Database\Factories;

use App\Enum\Category;
use App\Models\Category as CategoryModel;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CategoryModel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            Category::PRIORITY => $this->faker->unique()->numberBetween(1, 1000),
            Category::IS_ACTIVE => $this->faker->boolean,
            Category::SLUG => Str::slug(Str::random(10)),
            Category::NAME => $this->faker->sentence(2),
        ];
    }

    /**
     * @return CategoryFactory
     */
    public function internationalPaymentCategory(): CategoryFactory
    {
        return $this->state(function(){
            return [
                Category::SLUG => 'international_payments',
                Category::NAME => 'پرداخت های بین الملل',
                Category::PRIORITY => 1,
                Category::IS_ACTIVE => true,
            ];
        });
    }

    /**
     * @return CategoryFactory
     */
    public function deactiveCategory(): CategoryFactory
    {
        return $this->state(function(){
            return [
                Category::IS_ACTIVE => false,
            ];
        });
    }

    /**
     * @return CategoryFactory
     */
    public function lowPriorityCategory(): CategoryFactory
    {
        return $this->state(function(){
            return [
                Category::PRIORITY => 2000,
            ];
        });
    }

    /**
     * @return CategoryFactory
     */
    public function longSlugCategory(): CategoryFactory
    {
        return $this->state(function(){
            return [
                Category::SLUG => $this->faker->sentence(200)
            ];
        });
    }

    /**
     * @return CategoryFactory
     */
    public function emptySlugCategory(): CategoryFactory
    {
        return $this->state(function(){
            return [
                Category::SLUG => null
            ];
        });
    }

    /**
     * @return CategoryFactory
     */
    public function longNameCategory(): CategoryFactory
    {
        return $this->state(function(){
            return [
                Category::NAME => $this->faker->sentence(200)
            ];
        });
    }

    /**
     * @return CategoryFactory
     */
    public function emptyNameCategory(): CategoryFactory
    {
        return $this->state(function(){
            return [
                Category::NAME => null
            ];
        });
    }

    /**
     * @return CategoryFactory
     */
    public function shortSlugCategory(): CategoryFactory
    {
        return $this->state(function(){
            return [
                Category::SLUG => $this->faker->lexify('??')
            ];
        });
    }

    /**
     * @return CategoryFactory
     */
    public function shortNameCategory(): CategoryFactory
    {
        return $this->state(function(){
            return [
                Category::NAME => $this->faker->lexify('??')
            ];
        });
    }

    /**
     * @return CategoryFactory
     */
    public function invalidIsActiveCategory(): CategoryFactory
    {
        return $this->state(function () {
            return [
                Category::IS_ACTIVE => $this->faker->word
            ];
        });
    }

    /**
     * @return CategoryFactory
     */
    public function emptyIsActiveCategory(): CategoryFactory
    {
        return $this->state(function () {
            return [
                Category::IS_ACTIVE => null
            ];
        });
    }

    /**
     * @return CategoryFactory
     */
    public function stringPriorityCategory(): CategoryFactory
    {
        return $this->state(function () {
            return [
                Category::PRIORITY => $this->faker->word
            ];
        });
    }

    /**
     * @return CategoryFactory
     */
    public function outOfRangePriorityCategory(): CategoryFactory
    {
        return $this->state(function () {
            return [
                Category::PRIORITY => $this->faker->numberBetween(10001)
            ];
        });
    }

    /**
     * @return CategoryFactory
     */
    public function stringParentValue(): CategoryFactory
    {
        return $this->state(function () {
            return [
                Category::PARENT => $this->faker->word
            ];
        });
    }

    /**
     * @return CategoryFactory
     */
    public function invalidParentValue(): CategoryFactory
    {
        return $this->state(function () {
            return [
                Category::PARENT => $this->faker->numberBetween(1000)
            ];
        });
    }
}
