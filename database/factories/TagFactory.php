<?php

namespace Database\Factories;

use App\Enum\Tag;
use App\Models\Tag as TagModel;
use Illuminate\Database\Eloquent\Factories\Factory;

class TagFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TagModel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            Tag::NAME => $this->faker->title,
            Tag::IS_ACTIVE => $this->faker->boolean,
            Tag::COLOR => $this->faker->colorName,
        ];
    }

    /**
     * @return TagFactory
     */
    public function registerOrderTag(): TagFactory
    {
        return $this->state(function(){
            return [
                Tag::NAME => 'ثبت سفارش',
                Tag::IS_ACTIVE => true,
                Tag::COLOR => $this->faker->colorName,
            ];
        });
    }
}
