<?php

namespace Database\Factories;

use App\Enum\Login;
use App\Models\Login as LoginModel;
use Illuminate\Database\Eloquent\Factories\Factory;

class LoginFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LoginModel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            Login::IP => $this->faker->ipv4,
            Login::PLATFORM => Login::$platform[rand(0,2)]
        ];
    }
}
