<?php

use App\Enum\Login;
use App\Enum\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Login::TABLE, function (Blueprint $table) {
            $table->id(Login::LOGIN_ID);
            $table->unsignedBigInteger(Login::FK_USER_ID);
            $table->string(Login::IP);
            $table->enum(Login::PLATFORM, Login::$platform)->default(Login::WEB)->comment(implode(', ', Login::$platform));
            $table->timestamps();
        });

        Schema::table(Login::TABLE, function(Blueprint $table) {
            $table->foreign(Login::FK_USER_ID)->references(User::USER_ID)->on(User::TABLE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Login::TABLE);
    }
}
