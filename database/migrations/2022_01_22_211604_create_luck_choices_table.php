<?php

use App\Enum\Luck;
use App\Enum\LuckChoice;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLuckChoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(LuckChoice::TABLE, function (Blueprint $table) {
            $table->id(LuckChoice::LUCK_CHOICE_ID);
            $table->unsignedBigInteger(LuckChoice::FK_LUCK_ID);
            $table->string(LuckChoice::VALUE);
            $table->enum(LuckChoice::TYPE,LuckChoice::$luckChoiceType);
            $table->timestamps();
        });

        Schema::table(LuckChoice::TABLE, function(Blueprint $table) {
            $table->foreign(LuckChoice::FK_LUCK_ID)->references(Luck::LUCK_ID)->on(Luck::TABLE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(LuckChoice::TABLE);
    }
}
