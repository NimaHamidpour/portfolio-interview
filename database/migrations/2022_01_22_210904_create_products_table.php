<?php

use App\Enum\Business;
use App\Enum\Category;
use App\Enum\Product;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Product::TABLE, function (Blueprint $table) {
            $table->id(Product::PRODUCT_ID);
            $table->string(Product::SKU);
            $table->unsignedBigInteger(Product::FK_BUSINESS_ID);
            $table->unsignedBigInteger(Product::FK_CATEGORY_ID);
            $table->string(Product::BRAND);
            $table->integer(Product::QUANTITY)->default(1);
            $table->integer(Product::PRICE);
            $table->float(Product::DISCOUNT_PERCENT)->default(0);
            $table->text(Product::DESCRIPTION);
            $table->text(Product::DETAIL);
            $table->text(Product::COLOR)->nullable();
            $table->boolean(Product::ALLOW_COMMENT)->default(true);
            $table->timestamps();
            $table->softDeletes();

        });

        Schema::table(Product::TABLE, function(Blueprint $table) {
            $table->foreign(Product::FK_BUSINESS_ID)->references(Business::BUSINESS_ID)->on(Business::TABLE);
            $table->foreign(Product::FK_CATEGORY_ID)->references(Category::CATEGORY_ID)->on(Category::TABLE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Product::TABLE);
    }
}
