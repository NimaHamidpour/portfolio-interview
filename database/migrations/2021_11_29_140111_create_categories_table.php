<?php

use App\Enum\Category;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Category::TABLE, function (Blueprint $table) {
            $table->id(Category::CATEGORY_ID);
            $table->string(Category::SLUG);
            $table->string(Category::NAME);
            $table->integer(Category::PRIORITY)->default(0);
            $table->boolean(Category::IS_ACTIVE)->default(true);
            $table->nestedSet();
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Category::TABLE);
    }
}
