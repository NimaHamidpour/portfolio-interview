<?php

use App\Enum\Config;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Config::TABLE, function (Blueprint $table) {
            $table->id(Config::ID);
            $table->string(Config::KEY)->unique();
            $table->text(Config::VALUE);
            $table->string(Config::GROUP)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Config::TABLE);
    }
}
