<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use \App\Enum\User;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(User::TABLE, function (Blueprint $table) {
            $table->bigIncrements(User::USER_ID);
            $table->string(User::AVATAR)->nullable();
            $table->string(User::USERNAME)->unique()->nullable();
            $table->string(User::FIRST_NAME)->nullable();
            $table->string(User::LAST_NAME)->nullable();
            $table->string(User::MOBILE)->unique();
            $table->string(User::NATIONAL_CODE)->nullable()->unique();
            $table->string(User::EMAIL)->nullable()->unique();
            $table->enum(User::GENDER,User::$genderTypes)->nullable()
                ->comment(implode(', ', USER::$genderTypes));
            $table->string(User::PROVINCE)->nullable();
            $table->string(User::ADDRESS)->nullable();
            $table->string(User::ZIPCODE)->nullable();
            $table->date(User::BIRTH_DATE)->nullable();
            $table->string(User::REGISTER_IP,15)->nullable();
            $table->string(User::AFFILIATE_CODE)->nullable();
            $table->tinyInteger(User::STATUS)->nullable();
            $table->tinyInteger(User::ALLOW_CHARGE)->nullable();
            $table->boolean(User::LOCK_BALANCE)->nullable();
            $table->boolean(User::LOCK_WITHDRAW)->nullable();
            $table->string(User::PASSWORD)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
      }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(User::TABLE);
    }
}
