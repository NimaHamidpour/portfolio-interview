<?php

use App\Enum\Bank;
use App\Enum\User;
use App\Enum\UserBank;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(UserBank::TABLE, function (Blueprint $table) {
            $table->id(UserBank::USER_BANK_ID);
            $table->unsignedBigInteger(UserBank::FK_USER_ID);
            $table->string(UserBank::BANK_NAME);
            $table->string(UserBank::OWNER_NAME);
            $table->string(UserBank::CARD_NUMBER);
            $table->string(UserBank::SHEBA);
            $table->timestamps();
        });

        Schema::table(UserBank::TABLE, function(Blueprint $table) {
            $table->foreign(UserBank::FK_USER_ID)->references(User::USER_ID)->on(User::TABLE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(UserBank::TABLE);
    }
}
