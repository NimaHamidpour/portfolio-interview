<?php

use App\Enum\Coupon;
use App\Enum\Business;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Coupon::TABLE, function (Blueprint $table) {
            $table->id(Coupon::COUPON_ID);
            $table->unsignedBigInteger(Coupon::FK_BUSINESS_ID);
            $table->string(Coupon::TITLE);
            $table->string(Coupon::CODE)->unique();
            $table->string(Coupon::PERCENT);
            $table->integer(Coupon::QUANTITY);
            $table->integer(Coupon::USED);
            $table->timestamp(Coupon::EXPIRED_AT);
            $table->timestamps();
        });

        Schema::table(Coupon::TABLE, function(Blueprint $table) {
            $table->foreign(Coupon::FK_BUSINESS_ID)->references(Business::BUSINESS_ID)->on(Business::TABLE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Coupon::TABLE);
    }
}
