<?php

use App\Enum\Business;
use App\Enum\Category;
use App\Enum\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Business::TABLE, function (Blueprint $table) {
            $table->id(Business::BUSINESS_ID);
            $table->unsignedBigInteger(Business::FK_USER_ID);
            $table->unsignedBigInteger(Business::FK_CATEGORY_ID);
            $table->unsignedBigInteger(Business::FK_CITY_ID);
            $table->string(Business::TITLE);
            $table->string(Business::SLUG)->unique();
            $table->text(Business::DESCRIPTION)->nullable();
            $table->string(Business::REGION)->nullable();
            $table->string(Business::MAP)->nullable();
            $table->string(Business::MOBILE,11)->nullable();
            $table->string(Business::PHONE,15)->nullable();
            $table->string(Business::WEBSITE)->nullable();
            $table->string(Business::WHATSAPP)->nullable();
            $table->string(Business::TELEGRAM)->nullable();
            $table->string(Business::INSTAGRAM)->nullable();
            $table->string(Business::EMAIL)->nullable();
            $table->string(Business::WALLET)->default(0);
            $table->boolean(Business::ALLOW_CHARGE)->default(true);
            $table->boolean(Business::LOCK_BALANCE)->default(false);
            $table->boolean(Business::LOCK_WITHDRAW)->default(false);
            $table->enum(Business::STATUS,Business::$statusTYpe)->default(Business::PENDING);
            $table->timestamps();
            $table->softDeletes();

        });

        Schema::table(Business::TABLE, function(Blueprint $table) {
            $table->foreign(Business::FK_USER_ID)->references(User::USER_ID)->on(User::TABLE);
            $table->foreign(Business::FK_CATEGORY_ID)->references(Category::CATEGORY_ID)->on(Category::TABLE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Business::TABLE);
    }
}
