<?php

use App\Enum\Business;
use App\Enum\Luck;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLucksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Luck::TABLE, function (Blueprint $table) {
            $table->id(Luck::LUCK_ID);
            $table->unsignedBigInteger(Luck::FK_BUSINESS_ID);
            $table->string(Luck::TITLE);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table(Luck::TABLE, function(Blueprint $table) {
            $table->foreign(Luck::FK_BUSINESS_ID)->references(Business::BUSINESS_ID)->on(Business::TABLE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Luck::TABLE);
    }
}
