<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (config('developers') as $user) {
            $user['password'] = bcrypt($user['password']);
            $role = $user['role'];
            unset($user['role']);

            $user = User::firstOrCreate([
                \App\Enum\User::MOBILE => $user['mobile']
            ], $user);
        }

    }
}
