<?php

namespace Database\Seeders;

use App\Enum\User as EnumUser;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (isset(config('developers')[0]['mobile']))
        {
            $user = User::firstOrCreate([
                     EnumUser::MOBILE => config('developers')[0]['mobile']
                ],
                [
                    EnumUser::USERNAME      => config('developers')[0]['username'],
                    EnumUser::PASSWORD    => Hash::make(config('developers')[0]['password']),
                    EnumUser::REGISTER_IP => '192.168.0.1'
                ],
            );
        }
    }
}
