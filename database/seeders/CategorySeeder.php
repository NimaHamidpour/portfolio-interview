<?php

namespace Database\Seeders;

use App\Enum\User as EnumUser;
use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CategorySeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       $firstNode = Category::create([
           'name' => 'لوازم الکترونیکی',
           'slug' => 'electronic',
           'priority' => '1',
       ]);

        $secondNode =  Category::create([
            'name' => 'موبایل',
            'slug' => 'mobile',
            'priority' => '2',
            'parent_id' => $firstNode->category_id,
        ]);

        Category::create([
            'name' => 'سامسونگ',
            'slug' => 'samsung',
            'priority' => '2',
            'parent_id' => $secondNode->category_id,
        ]);
    }
}
