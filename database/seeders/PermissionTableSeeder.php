<?php
namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $guardName = 'api';
        $routes = [];
        $allRoutes = [];

        foreach (app('router')->getRoutes() as $route) {
            $routeName = $route->getName();
            if(preg_match('/^(admin|api|public)\.\w+\.?(.*)?/', $routeName)) {
                $allRouteName = makeAllRouteName($routeName);

                if(!in_array($allRouteName, $allRoutes)) {
                    $allRoutes[] = $allRouteName;
                    $routes[] = [
                        'name' => $allRouteName,
                        'guard_name' => $guardName,
                        'created_at' => Carbon::now()
                    ];
                }

                $routes[] = [
                    'name' => $routeName,
                    'guard_name' => $guardName,
                    'created_at' => Carbon::now()
                ];
            }
        }

        Permission::insertOrIgnore($routes);
    }
}
