<?php

namespace App\Events;

use App\Interfaces\Models\UserInterface;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class VerificationEmailSentEvent
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    /**
     * @param string $userEmail      UserEmail.
     * @param string $emailVerifyUrl EmailVerifyUrl.
     */
    public function __construct(public string $userEmail, public string $emailVerifyUrl)
    {
    }
}
