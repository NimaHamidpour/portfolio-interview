<?php

namespace App\Events;

use App\Interfaces\Models\UserInterface;
use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserLoggedInEvent
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @param UserInterface $user     User.
     * @param string        $ip       Ip.
     * @param string|null   $platform Platform.
     */
    public function __construct(public UserInterface $user, public string $ip, public string|null $platform)
    {
    }
}
