<?php

namespace App\Events;

use App\Interfaces\Models\UserInterface;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserRolePermissionCacheClearedEvent
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @param UserInterface $user User.
     */
    public function __construct(public UserInterface $user)
    {
    }
}
