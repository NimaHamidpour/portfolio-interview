<?php

namespace App\Events;

use App\Interfaces\Models\UserInterface;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserUpdatedEvent
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    /**
     * @var UserInterface
     */
    public UserInterface $user;

    /**
     * Create a new event instance.
     *
     * @param UserInterface $user User.
     */
    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }
}
