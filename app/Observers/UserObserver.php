<?php

namespace App\Observers;

use App\Events\UserRolePermissionCacheClearedEvent;
use App\Models\User;

class UserObserver
{
    /**
     * Handle the User "updated" event.
     *
     * @param User $user User.
     * @return void
     */
    public function updated(User $user)
    {
        event(new UserRolePermissionCacheClearedEvent($user));
    }
}
