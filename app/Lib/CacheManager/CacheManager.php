<?php

namespace App\Lib\CacheManager;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

class CacheManager implements CacheManagerInterface
{
    /**
     * @var boolean
     */
    private static bool $dontCache = false;

    const PREFIX = 'odin:';

    /**
     * @param string       $key      Key.
     * @param callable     $callback Callback.
     * @param integer|null $ttl      TTL.
     * @return mixed
     */
    public static function getFromCache(string $key, callable $callback, int $ttl = null): mixed
    {
        $key = static::PREFIX . $key;

        if ($value = Cache::get($key)) {
            return static::unSerializeValue($value);
        }

        $result = call_user_func($callback, new static());

        if (static::$dontCache) {
            return $result;
        }

        Cache::add($key, static::serializeValue($result), $ttl);

        return $result;
    }

    /**
     * @param string $key Key.
     * @return void
     */
    public static function forgetByKey(string $key): void
    {
        Cache::forget($key);
    }

    /**
     * @param mixed ...$patterns Patterns.
     * @return void
     */
    public static function forgetByPattern(mixed ...$patterns): void
    {
        $cachePrefix = config('database.redis.options.prefix');
        foreach ($patterns as $pattern) {
            $keys = Redis::keys($pattern);

            foreach ($keys as $key) {
                $key = str_replace($cachePrefix, '', $key);
                Redis::del($key);
            }
        }
    }

    /**
     * @return void
     */
    public function disableCache(): void
    {
        static::$dontCache = true;
    }

    /**
     * @return void
     */
    public function enableCache(): void
    {
        static::$dontCache = false;
    }

    /**
     * @param mixed $value Value.
     * @return string
     */
    protected static function serializeValue(mixed $value): string
    {
        return is_numeric($value) && $value != 0 ? $value : serialize($value);
    }

    /**
     * @param mixed $value Value.
     * @return mixed
     */
    protected static function unSerializeValue(mixed $value): mixed
    {
        return is_numeric($value) && $value != 0 ? $value : unserialize($value);
    }
}
