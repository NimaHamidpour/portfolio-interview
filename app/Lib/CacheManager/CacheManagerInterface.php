<?php

namespace App\Lib\CacheManager;

interface CacheManagerInterface
{
    /**
     * @param string       $key      Key.
     * @param callable     $callback Callback.
     * @param integer|null $ttl      TTL.
     * @return mixed
     */
    public static function getFromCache(string $key, callable $callback, int $ttl = null): mixed;

    /**
     * @return void
     */
    public function disableCache(): void;

    /**
     * @return void
     */
    public function enableCache(): void;

    /**
     * @param string $key Key.
     * @return void
     */
    public static function forgetByKey(string $key): void;

    /**
     * @param string $pattern Pattern.
     * @return void
     */
    public static function forgetByPattern(string $pattern): void;
}
