<?php

namespace App\Lib\UserVerification\Verifications;

use App\Enum\UserLegalInfo;
use App\Enum\Verification;
use App\Exceptions\OdinException;
use App\Lib\UserVerification\VerificationAbstract;
use App\Models\User;

class LegalInfoVerification extends VerificationAbstract
{
    /**
     * @param User $user User.
     */
    public function __construct(User $user)
    {
        $this->setUser($user);
    }

    /**
     * @param array $data Data.
     * @return mixed
     * @throws OdinException OdinException.
     */
    public function addVerification(array $data): mixed
    {
        $this->checkUserVerification($this->getVerificationType());

        $userData = [
            'companyEconomicCode' => $data[UserLegalInfo::COMPANY_ECONOMIC_CODE],
            'companyTaxCode' => $data[UserLegalInfo::COMPANY_TAX_CODE],
            'companyRegistrationNumber' => $data[UserLegalInfo::COMPANY_REGISTRATION_NUMBER]
        ];

        $verificationItem = $this->makePendingVerificationItem($userData);
        return $this->createVerification($verificationItem);
    }

    /**
     * @return string
     */
    public function getVerificationType(): string
    {
        return Verification::LEGAL_INFO;
    }
}
