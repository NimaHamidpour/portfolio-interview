<?php

namespace App\Lib\UserVerification\Verifications;

use App\Enum\Bank;
use App\Enum\UserBank;
use App\Enum\Verification;
use App\Exceptions\OdinException;
use App\Lib\UserVerification\VerificationAbstract;
use App\Models\User;

class BankCardVerification extends VerificationAbstract
{
    /**
     * @param User $user User.
     */
    public function __construct(User $user)
    {
        $this->setUser($user);
    }

    /**
     * @param array $data Data.
     * @return mixed
     * @throws OdinException OdinException.
     */
    public function addVerification(array $data): mixed
    {
        $this->checkUserVerification($this->getVerificationType());
        $userData = [
            'bankId' => $data[Bank::BANK_ID],
            'cardNumber' => $data[UserBank::CARD_NUMBER],
            'accountNumber' => $data[UserBank::ACCOUNT_NUMBER],
            'sheba' => $data[UserBank::SHEBA],
        ];

        $verificationItem = $this->makePendingVerificationItem($userData);
        return $this->createVerification($verificationItem);
    }

    /**
     * @return string
     */
    public function getVerificationType(): string
    {
        return Verification::BANK_CARD;
    }
}
