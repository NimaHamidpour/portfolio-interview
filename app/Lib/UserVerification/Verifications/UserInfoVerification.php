<?php

namespace App\Lib\UserVerification\Verifications;

use App\Enum\User as UserEnum;
use App\Enum\Verification;
use App\Exceptions\OdinException;
use App\Lib\UserVerification\VerificationAbstract;
use App\Models\User;

class UserInfoVerification extends VerificationAbstract
{
    /**
     * @param User $user User.
     */
    public function __construct(User $user)
    {
        $this->setUser($user);
    }

    /**
     * @param array $data Data.
     * @return mixed
     * @throws OdinException OdinException.
     */
    public function addVerification(array $data): mixed
    {
        $this->checkUserVerification($this->getVerificationType());

        $userData = [
            'nationalCode' => $data[UserEnum::NATIONAL_CODE],
            'gender' => $data[UserEnum::GENDER],
            'birthdate' => $data[UserEnum::BIRTH_DATE],
            'zipcode' => $data[UserEnum::ZIPCODE],
            'province' => $data[UserEnum::PROVINCE],
            'address' => $data[UserEnum::ADDRESS]
        ];

        $verificationItem = $this->makePendingVerificationItem($userData);
        return $this->createVerification($verificationItem);
    }

    /**
     * @return string
     */
    public function getVerificationType(): string
    {
        return Verification::USER_INFO;
    }
}
