<?php

namespace App\Lib\UserVerification\Verifications;

use App\Enum\Verification;
use App\Exceptions\OdinException;
use App\Lib\UserVerification\VerificationAbstract;
use App\Models\User;

class CustomIdSecondVerification extends VerificationAbstract
{
    /**
     * @param User $user User.
     */
    public function __construct(User $user)
    {
        $this->setUser($user);
    }

    /**
     * @param array $data Data.
     * @return mixed
     * @throws OdinException OdinException.
     */
    public function addVerification(array $data): mixed
    {
        $this->checkUserVerification($this->getVerificationType());

        $userData = [
            'customIDSecond' => 1,
        ];

        $clientDescription = !empty($data['description']) ? $data['description'] : null;
        $verificationItem = $this->makePendingVerificationItem($userData, $clientDescription);
        return $this->createVerification($verificationItem);
    }

    /**
     * @return string
     */
    public function getVerificationType(): string
    {
        return Verification::CUSTOM_ID_SECOND;
    }
}
