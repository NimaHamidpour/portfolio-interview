<?php

namespace App\Lib\UserVerification\Verifications;

use App\Enum\User as UserEnum;
use App\Enum\Verification;
use App\Events\VerificationEmailSentEvent;
use App\Exceptions\OdinException;
use App\Lib\UserVerification\VerificationAbstract;
use App\Models\User;
use Carbon\Carbon;

class EmailVerification extends VerificationAbstract
{
    /**
     * @param User $user User.
     */
    public function __construct(User $user)
    {
        $this->setUser($user);
    }

    /**
     * @param array $data Data.
     * @return mixed
     * @throws OdinException OdinException.
     */
    public function addVerification(array $data): mixed
    {
        $this->checkUserVerification(
            $this->getVerificationType(),
            Carbon::now()->addHours(config('services.verification.email_verification_expiration_timestamp'))
        );

        $userData = [
            'email' => $data[UserEnum::EMAIL],
        ];

        $clientDescription = !empty($data['description']) ? $data['description'] : null;
        $verificationItem = $this->makePendingVerificationItem($userData, $clientDescription);
        $emailVerification = $this->createVerification($verificationItem);
        $emailVerifyUrl = $this->makeEmailVerifyUrl($emailVerification->verification_id);
        event(new VerificationEmailSentEvent($userData['email'], $emailVerifyUrl));
        return $emailVerification;
    }

    /**
     * @return string
     */
    public function getVerificationType(): string
    {
        return Verification::EMAIL;
    }

    /**
     * @param integer $verificationId VerificationId.
     * @return string
     */
    private function makeEmailVerifyUrl(int $verificationId): string
    {
        $baseUrl = config('services.verification.email_verification_verify_url');
        $expirationHour = config('services.verification.email_verification_expiration_timestamp');
        $creationDateTime = Carbon::now();
        $expirationTimestamp = $creationDateTime->addHours($expirationHour)->timestamp;

        $token = encrypt(json_encode([
            'verificationId' => $verificationId,
            'creationTimestamp' => $creationDateTime->timestamp,
            'expirationTimestamp' => $expirationTimestamp,
        ]));

        return sprintf('%s?code=%s', $baseUrl, $token);
    }
}
