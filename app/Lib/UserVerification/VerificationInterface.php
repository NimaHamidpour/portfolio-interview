<?php

namespace App\Lib\UserVerification;

use App\Models\User;

interface VerificationInterface
{
    /**
     * @param  array $verificationData VerificationData.
     * @return mixed
     */
    public function addVerification(array $verificationData): mixed;

    /**
     * @return string
     */
    public function getVerificationType(): string;
}
