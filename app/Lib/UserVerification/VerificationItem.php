<?php

namespace App\Lib\UserVerification;

use App\Models\UserBank;
use App\Models\User;
use App\Models\UserLegalInfo;

class VerificationItem
{
    /**
     * @var User
     */
    public User $user;

    /**
     * @var UserBank
     */
    public $userBank;

    /**
     * @var  string
     */
    public $type;

    /**
     * @var  string
     */
    public $status;

    /**
     * @var  string
     */
    public $value;

    /**
     * @var  string
     */
    public $rejectReason;

    /**
     * @var  string
     */
    public $clientDescription;

    /**
     * @var integer
     */
    public $verifiedBy;

    /**
     * @var integer
     */
    public $verifiedAt;

    /**
     * @param User $user User.
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
