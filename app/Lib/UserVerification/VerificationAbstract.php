<?php

namespace App\Lib\UserVerification;

use App\Enum\Verification as EnumVerification;
use App\Exceptions\OdinException;
use App\Models\User;
use App\Models\Verification;
use Carbon\Carbon;
use Illuminate\Http\Response;

abstract class VerificationAbstract implements VerificationInterface
{
    /**
     * @var user User.
     */
    protected User $user;

    /**
     * @param VerificationItem $verificationItem VerificationItem.
     * @return Verification
     */
    public function createVerification(VerificationItem $verificationItem): Verification
    {
        $verification = new Verification();
        $verification->fk_user_id = $verificationItem->user->user_id;
        $verification->verification_type = $verificationItem->type;
        $verification->status = $verificationItem->status ?? EnumVerification::PENDING;
        $verification->value = $verificationItem->value;
        $verification->client_description = $verificationItem->clientDescription;
        $verification->verified_by = $verificationItem->verifiedBy;
        $verification->verified_at = $verificationItem->verifiedAt;
        $verification->reject_reason = $verificationItem->rejectReason;
        $verification->save();

        return $verification;
    }

    /**
     * @param Verification     $verification     Verification.
     * @param VerificationItem $verificationItem VerificationItem.
     * @return boolean
     */
    public function updateVerification(Verification $verification, VerificationItem $verificationItem): bool
    {
        $verification->status = $verificationItem->status;
        $verification->verified_by = $verificationItem->verifiedBy;
        $verification->verified_at = Carbon::now();
        $verification->reject_reason = $verificationItem->rejectReason;

        return $verification->save();
    }

    /**
     * @param string      $verificationType            VerificationType.
     * @param string|null $verificationExpireTimestamp VerificationTimestamp.
     * @return void
     * @throws OdinException OdinException.
     */
    public function checkUserVerification(string $verificationType, ?string $verificationExpireTimestamp = null): void
    {
        $userVerification = Verification::select('status')
            ->where(EnumVerification::FK_USER_ID, $this->user->user_id)
            ->where(EnumVerification::VERIFICATION_TYPE, $verificationType)
            ->whereIn(EnumVerification::STATUS, [EnumVerification::PENDING, EnumVerification::ACCEPTED])
            ->when(!empty($verificationExpireTimestamp), function ($query) use ($verificationExpireTimestamp) {
                return $query->where('created_at', '<', $verificationExpireTimestamp);
            })
            ->get()
            ->last();

        if (!empty($userVerification)) {
            throw new OdinException(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                __(
                    'messages.verification.verification_already_exists',
                    ['value' => __(sprintf('messages.verification.%s', strtolower($userVerification['status'])))]
                )
            );
        }
    }

    /**
     * @param User $user User.
     * @return void
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param array       $userData          UserData.
     * @param string|null $clientDescription ClientDescription.
     * @return VerificationItem
     */
    protected function makePendingVerificationItem(array $userData, ?string $clientDescription = null): VerificationItem
    {
        $verificationItem = new VerificationItem($this->getUser());
        $verificationItem->clientDescription = $clientDescription;
        $verificationItem->type = $this->getVerificationType();
        $verificationItem->status = EnumVerification::PENDING;
        $verificationItem->value = json_encode($userData, JSON_UNESCAPED_UNICODE);
        return $verificationItem;
    }
}
