<?php

namespace App\Lib\UserVerification;

use App\Enum\Verification as EnumVerification;
use App\Lib\UserVerification\Verifications\BankCardVerification;
use App\Lib\UserVerification\Verifications\CustomIdFirstVerification;
use App\Lib\UserVerification\Verifications\CustomIdSecondVerification;
use App\Lib\UserVerification\Verifications\EmailVerification;
use App\Lib\UserVerification\Verifications\IdCardBackVerification;
use App\Lib\UserVerification\Verifications\IdCardFrontVerification;
use App\Lib\UserVerification\Verifications\LegalInfoVerification;
use App\Lib\UserVerification\Verifications\PhoneVerification;
use App\Lib\UserVerification\Verifications\SelfieVerification;
use App\Lib\UserVerification\Verifications\UserInfoVerification;
use App\Lib\UserVerification\Verifications\VideoVerification;
use App\Models\User;

class VerificationFactory
{
    /**
     * @param string $verificationType VerificationType.
     * @param User   $user             User.
     * @return VerificationInterface
     */
    public static function makeVerification(string $verificationType, User $user): VerificationInterface
    {
        return match ($verificationType) {
            EnumVerification::USER_INFO         =>  new UserInfoVerification($user),
            EnumVerification::PHONE             =>  new PhoneVerification($user),
            EnumVerification::LEGAL_INFO        =>  new LegalInfoVerification($user),
            EnumVerification::BANK_CARD         =>  new BankCardVerification($user),
            EnumVerification::VIDEO             =>  new VideoVerification($user),
            EnumVerification::USER_SELFIE       =>  new SelfieVerification($user),
            EnumVerification::EMAIL             =>  new EmailVerification($user),
            EnumVerification::ID_CARD_FRONT     =>  new IdCardFrontVerification($user),
            EnumVerification::ID_CARD_BACK      =>  new IdCardBackVerification($user),
            EnumVerification::CUSTOM_ID_FIRST   =>  new CustomIdFirstVerification($user),
            EnumVerification::CUSTOM_ID_SECOND  =>  new CustomIdSecondVerification($user),
        };
    }
}
