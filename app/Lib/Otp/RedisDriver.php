<?php

namespace App\Lib\Otp;

use App\Lib\Otp\Interfaces\Driver;
use Illuminate\Support\Facades\Redis;

class RedisDriver implements Driver
{
    /**
     * @param string $key Key.
     * @return integer
     */
    public function get(string $key): int|null
    {
        return Redis::get($key);
    }

    /**
     * @param string  $key   Key.
     * @param integer $value Value.
     * @return boolean
     */
    public function set(string $key, int $value): bool
    {
        return Redis::set($key, $value);
    }

    /**
     * @param string  $key   Key.
     * @param integer $value Value.
     * @param integer $ttl   Time To Live.
     * @return void
     */
    public function setWithExpiration(string $key, int $value, int $ttl): void
    {
        Redis::setex($key, $ttl, $value);
    }

    /**
     * @param string $key Key.
     * @return integer
     */
    public function getTimeToLive(string $key): int
    {
        return Redis::ttl($key);
    }
}
