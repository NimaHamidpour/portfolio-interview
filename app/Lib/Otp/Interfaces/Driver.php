<?php

namespace App\Lib\Otp\Interfaces;

interface Driver
{
    /**
     * @param string $key Key.
     * @return integer|null
     */
    public function get(string $key): int|null;

    /**
     * @param string  $key   Key.
     * @param integer $value Value.
     * @return boolean
     */
    public function set(string $key, int $value): bool;

    /**
     * @param string  $key   Key.
     * @param integer $value Value.
     * @param integer $ttl   Time To Live.
     * @return void
     */
    public function setWithExpiration(string $key, int $value, int $ttl): void;

    /**
     * @param string $key Key.
     * @return integer
     */
    public function getTimeToLive(string $key): int;
}
