<?php

namespace App\Lib\Otp;

use App\Lib\Otp\Interfaces\Driver;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class Otp
{
    /**
     * @var string
     */
    private static string $otpKey = 'otp:register:%s';

    /**
     * @var string
     */
    private static string $countOfTriesKey = 'otp:tries:%s';

    /**
     * @var Driver
     */
    private Driver $driver;

    /**
     * @param Driver $driver Driver Interface.
     * @return self
     */
    public function setDriver(Driver $driver): self
    {
        $this->driver = $driver;
        return $this;
    }


    /**
     * @param string $key Key.
     * @return integer
     * @throws Exception Throwable.
     */
    protected function checkTtl(string $key): int
    {
        $ttl = $this->driver->getTimeToLive($key);
        abort_if($ttl > 0, Response::HTTP_FORBIDDEN, sprintf(__('auth.otp_ttl'), $ttl));

        return $ttl;
    }

    /**
     * @param string $key Key.
     * @return void
     *
     * @throws Exception Throwable.
     */
    protected function checkCountOfTries(string $key): void
    {
        $countOfTry = $this->driver->get($key);
        if ($countOfTry && $countOfTry > config('auth.count_of_tries')) {
            abort(Response::HTTP_FORBIDDEN, __('auth.count_of_tries'));
        }

        $countOfTry = empty($countOfTry) ? 1 : ++$countOfTry;
        $this->driver->setWithExpiration($key, $countOfTry, config('auth.ttl_of_count_of_tries'));
    }

    /**
     * @param  string $key    Key.
     * @param  string $mobile Mobile.
     * @return integer
     */
    private function setRandomNumberForKey(string $key, string $mobile): int
    {
        $otp = rand(1000, 9999);
        if (in_array($mobile, array_column(config('developers'), 'mobile'))) {
            $otp = config('auth.default_otp') ?: rand(1000, 9999);
        }

        $this->driver->setWithExpiration($key, $otp, config('auth.ttl_of_otp'));
        Log::info(sprintf('random number for key %s is:  %s', $key, $otp));
        return $otp;
    }

    /**
     * @param string $mobile Mobile.
     * @return boolean
     * @throws Exception    Throwable.
     */
    public function setOtp(string $mobile): bool
    {
        $key = sprintf(self::$otpKey, $mobile);
        $otpKey = sprintf(self::$countOfTriesKey, $mobile);
        $this->checkTtl($key);
        $this->checkCountOfTries($otpKey);
        $this->setRandomNumberForKey($key, $mobile);

        return true;
    }

    /**
     * @param string  $mobile Mobile.
     * @param integer $otp    Otp.
     *
     * @return boolean
     */
    public function checkRandomNumberForKey(string $mobile, int $otp): bool
    {
        $key = sprintf(self::$otpKey, $mobile);

        if ($this->driver->get($key) < 0) {
            abort(Response::HTTP_UNAUTHORIZED, __('auth.otp_not_set'));
        }

        if ($this->driver->get($key) != $otp) {
            abort(Response::HTTP_FORBIDDEN, __('auth.otp_is_not_correct'));
        }

        return true;
    }
}
