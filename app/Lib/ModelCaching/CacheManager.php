<?php

namespace App\Lib\ModelCaching;

use Illuminate\Support\Facades\Redis;

class CacheManager
{
    /**
     * @var boolean Dont Cache flag
     */
    public static bool $dontCache = false;

    /**
     * @param string   $key      Key.
     * @param integer  $sec      Sec.
     * @param callable $callback Callback.
     *
     * @return mixed
     */
    public static function getFromCache(string $key, int $sec, callable $callback): mixed
    {
        $key = static::getCacheKey($key);

        if ($value = Redis::get($key)) {
            return static::unserialize($value);
        }

        $res = call_user_func($callback, new static());

        if (static::$dontCache == false) {
            Redis::setex($key, $sec, static::serialize($res));
        }

        return $res;
    }


    /**
     * @param string $key Key.
     *
     * @return mixed
     */
    public function getTTL(string $key): mixed
    {
        return Redis::ttl(static::getCacheKey($key));
    }


    /**
     * @param mixed $value Value.
     * @return integer|string
     */
    protected static function serialize(mixed $value): int|string
    {
        return is_numeric($value) ? $value : serialize($value);
    }


    /**
     * @param mixed $value Value.
     *
     * @return mixed
     */
    protected static function unserialize(mixed $value): mixed
    {
        return is_numeric($value) ? $value : unserialize($value);
    }

    /**
     * @return void
     */
    public function dontCache(): void
    {
        static::$dontCache = true;
    }

    /**
     * @return void
     */
    public function cache(): void
    {
        static::$dontCache = false;
    }

    /**
     * @param string $key Key.
     * @return string
     */
    private static function getCacheKey(string $key): string
    {
        return config('model-caching.cache_prefix') . $key;
    }
}
