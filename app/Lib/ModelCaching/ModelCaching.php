<?php

namespace App\Lib\ModelCaching;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use ReflectionException;

class ModelCaching
{
    /**
     * @var Model
     */
    protected Model $model;
    /**
     * @var Builder
     */
    protected Builder $builder;
    /**
     * @var array
     */
    protected array $methods = [];
    /**
     * @var array
     */
    protected array $parameters = [];
    /**
     * @var mixed
     */
    protected mixed $result;

    /**
     * @var integer
     */
    protected static int $cacheExpTime = 40;

    /**
     * ModelCaching constructor.
     * @param Model $model Eloquent Model.
     */
    public function __construct(Model $model)
    {
        static::$cacheExpTime = $model->cacheExpireTime ?? config('model-caching.cache_expire_time');
        $this->model = $model;
        $this->builder = $model->newQuery();
    }

    /**
     * @param string $method     Method.
     * @param mixed  $parameters Parameters.
     *
     * @return mixed
     * @throws ReflectionException  ReflectionException.
     */
    public function __call(string $method, mixed $parameters)
    {
        array_push($this->methods, $method);
        array_push($this->parameters, $this->processParameters($parameters));

        $cacheKey = $this->model->getTable() . ':' . $this->getCacheKey();

        return CacheManager::getFromCache($cacheKey, static::$cacheExpTime, function ($cacheManager) use ($method, $parameters) {

            $class = new \ReflectionClass($this->model);

            if ($class->hasMethod($method)) {
                $this->result = $this->model->$method(...$parameters);
            } else {
                $this->result = $this->builder->$method(...$parameters);
            }

            if ($this->result instanceof Model || $this->result instanceof Collection) {
                $cacheManager->cache();
                return $this->result;
            }

            $cacheManager->dontCache();

            return $this;
        });
    }

    /**
     * @param array $params Parameters.
     *
     * @return array
     * @throws ReflectionException  ReflectionException.
     */
    public function processParameters(array $params = []): array
    {
        return array_map(function ($item) {
            if (
                ($item instanceof \Closure) || (is_array($item)
                    &&
                    isset(array_values($item)[0])
                    &&
                    array_values($item)[0] instanceof \Closure)
            ) {
                $ref = new \ReflectionFunction($item);
                return get_class($ref->getClosureThis()) . $ref->getStartLine() . $ref->getEndLine();
            }
            return $item;
        }, $params);
    }

    /**
     * @return string
     */
    private function getCacheKey(): string
    {
        return md5(sprintf(
            'model_cache_%s_%s',
            serialize($this->methods),
            serialize($this->parameters)
        ));
    }
}
