<?php

namespace App\Lib\ModelCaching;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * @return void
     */
    public function boot(): void
    {
        $this->publishes([
            __DIR__ . '/../config/model-caching.php' => config_path('model-caching.php'),
        ]);
    }
}
