<?php

namespace App\Traits\Scopes;

use App\Enum\User;
use Illuminate\Database\Eloquent\Builder;

trait HasFullNameTrait
{
    /**
     * @param Builder $builder  Builder.
     * @param string  $fullName FullName.
     *
     * @return Builder
     */
    public function scopeWhereFullNameLike(Builder $builder, string $fullName): Builder
    {
        $userInfo = explode(' ', $fullName);
        return $builder->orWhere(User::FIRST_NAME, $userInfo[0])->orWhere(User::LAST_NAME, $userInfo[1]);
    }
}
