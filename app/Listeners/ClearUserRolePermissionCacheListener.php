<?php

namespace App\Listeners;

use App\Events\UserRolePermissionCacheClearedEvent;
use App\Lib\CacheManager\CacheManager;

class ClearUserRolePermissionCacheListener
{
    /**
     * Handle the event.
     *
     * @param  UserRolePermissionCacheClearedEvent $event Event.
     * @return void
     */
    public function handle(UserRolePermissionCacheClearedEvent $event)
    {
        $user = $event->user;
        $rolePattern = sprintf('%s%s%s', '*user:', $user->user_id, ':roles*');
        CacheManager::forgetByPattern($rolePattern);
    }
}
