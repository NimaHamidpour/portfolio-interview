<?php

namespace App\Listeners;

use App\Events\VerificationEmailSentEvent;
use App\Jobs\SendUserVerificationEmail;

class SendUserVerificationEmailListener
{
    /**
     * Handle the event.
     *
     * @param VerificationEmailSentEvent $event Event.
     * @return void
     */
    public function handle(VerificationEmailSentEvent $event)
    {
        dispatch(new SendUserVerificationEmail($event->userEmail, $event->emailVerifyUrl));
    }
}
