<?php

namespace App\Listeners;

use App\Events\UserLoggedInEvent;
use App\Jobs\StoreUserLoginInfo;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UserLoginListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  UserLoggedInEvent $event Event.
     * @return void
     */
    public function handle(UserLoggedInEvent $event)
    {
        dispatch(new StoreUserLoginInfo($event->user->user_id, $event->ip, $event->platform));
    }
}
