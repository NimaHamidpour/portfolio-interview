<?php

namespace App\Providers;

use App\Events\UserLoggedInEvent;
use App\Events\UserRolePermissionCacheClearedEvent;
use App\Events\UserUpdatedEvent;
use App\Listeners\ClearUserRolePermissionCacheListener;
use App\Listeners\SendUserUpdateNotificationListener;
use App\Listeners\UserLoginListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        UserUpdatedEvent::class => [
            SendUserUpdateNotificationListener::class
        ],
        UserRolePermissionCacheClearedEvent::class => [
            ClearUserRolePermissionCacheListener::class
        ],
        UserLoggedInEvent::class => [
            UserLoginListener::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
