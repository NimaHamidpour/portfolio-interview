<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

class UserBankFilter extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected array $filters = [
        'user_id'         => '=',
        'bank_name'       => '=',
        'sheba'           => '=',
        'card_number'     => '=',
        'owner_name'      => '=',
        'created_at'      => '=',
        'from_created_at' => '>=',
        'to_created_at'   => '<=',
    ];


    /**
     * Registered filters type to operate upon.
     *
     * @var array
     */
    protected array $filtersType = [
        'user_id'         => 'integer',
        'bank_name'       => 'string',
        'sheba'           => 'string',
        'card_number'     => 'string',
        'owner_name'      => 'string',
        'created_at'      => 'string',
        'from_created_at' => 'string',
        'to_created_at'   => 'string',
    ];


    /**
     * Registered map filters to operate upon.
     *
     * @var array
     */
    protected array $mapFilters = [
        'user_id'            => 'fk_user_id',
        'from_created_at'    => 'created_at',
        'to_created_at'      => 'created_at',
    ];
}
