<?php

namespace App\Filters;

class ConfigFilter extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected array $filters = [
        'key'             => '=',
        'value'           => '=',
        'group'           => '=',
        'from_created_at' => '>=',
        'to_created_at'   => '<=',
    ];

    /**
     * Registered filters type to operate upon.
     *
     * @var array
     */
    protected array $filtersType = [
        'key'             => 'string',
        'value'           => 'string',
        'group'           => 'string',
        'from_created_at' => 'string',
        'to_created_at'   => 'string',
    ];

    /**
     * Registered map filters to operate upon.
     *
     * @var array
     */
    protected array $mapFilters = [
        'from_created_at' => 'created_at',
        'to_created_at'   => 'created_at',
    ];
}
