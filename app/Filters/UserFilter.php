<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

class UserFilter extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected array $filters = [
        'user_id'         => '=',
        'username'        => '=',
        'first_name'      => 'like',
        'last_name'       => 'like',
        'national_code'   => 'like',
        'email'           => 'like',
        'mobile'          => 'like',
        'phone'           => 'like',
        'zipcode'         => 'like',
        'ip'              => 'like',
        'birthdate'       => '=',
        'from_birthdate'  => '>=',
        'to_birthdate'    => '<=',
        'created_at'      => '=',
        'from_created_at' => '>=',
        'to_created_at'   => '<=',
    ];

    /**
     * Registered custom filters to operate upon.
     *
     * @var array
     */
    protected array $customFilters = [
        'full_name',
    ];

    /**
     * Registered filters type to operate upon.
     *
     * @var array
     */
    protected array $filtersType = [
        'user_id'         => 'integer',
        'username'        => 'string',
        'first_name'      => 'string',
        'last_name'       => 'string',
        'national_code'   => 'string',
        'email'           => 'string',
        'mobile'          => 'string',
        'full_name'       => 'string',
        'phone'           => 'string',
        'zipcode'         => 'string',
        'ip'              => 'string',
        'birthdate'       => 'string',
        'from_birthdate'  => 'string',
        'to_birthdate'    => 'string',
        'created_at'      => 'string',
        'from_created_at' => 'string',
        'to_created_at'   => 'string',
    ];

    /**
     * Registered map filters to operate upon.
     *
     * @var array
     */
    protected array $mapFilters = [
        'ip'              => 'register_ip',
        'from_birthdate'  => 'birthdate',
        'to_birthdate'    => 'birthdate',
        'from_created_at' => 'created_at',
        'to_created_at'   => 'created_at',
        'full_name'       => 'fullName',
    ];

    /**
     * @param string $fullName FullName.
     *
     * @return Builder
     */
    protected function fullName(string $fullName): Builder
    {
        return $this->builder->WhereFullNameLike($fullName);
    }
}
