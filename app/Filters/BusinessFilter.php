<?php

namespace App\Filters;

use App\Enum\BaseEnum;
use App\Enum\Business;
use App\Enum\Category;
use App\Enum\User;

class BusinessFilter extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected array $filters = [
        Business::BUSINESS_ID            => '=',
        User::USER_ID                    => '=',
        Category::CATEGORY_ID            => '=',
        Business::TITLE                  => 'like',
        Business::SLUG                   => 'like',
        Business::REGION                 => 'like',
        Business::MOBILE                 => 'like',
        Business::PHONE                  => 'like',
        'from_created_at'                => '>=',
        'to_created_at'                  => '<=',
    ];

    /**
     * Registered filters type to operate upon.
     *
     * @var array
     */
    protected array $filtersType = [
        Business::BUSINESS_ID            => 'integer',
        Business::FK_USER_ID             => 'integer',
        Business::FK_CATEGORY_ID         => 'integer',
        Business::FK_CITY_ID             => 'integer',
        Business::TITLE                  => 'string',
        Business::SLUG                   => 'string',
        Business::REGION                 => 'string',
        Business::MOBILE                 => 'string',
        Business::PHONE                  => 'string',
        'from_created_at'                => 'string',
        'to_created_at'                  => 'string',
    ];

    /**
     * Registered map filters to operate upon.
     *
     * @var array
     */
    protected array $mapFilters = [
        'from_created_at'          => BaseEnum::CREATED_AT,
        'to_created_at'            => BaseEnum::CREATED_AT,
        User::USER_ID              => Business::FK_USER_ID,
        Category::CATEGORY_ID      => Business::FK_CATEGORY_ID,
    ];
}
