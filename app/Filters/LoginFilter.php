<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

class LoginFilter extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected array $filters = [
        'login_id'        => '=',
        'ip'              => '=',
        'platform'        => '=',
        'user_id'         => '=',
        'from_created_at' => '>=',
        'to_created_at'   => '<=',
    ];

    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected array $filtersType = [
        'login_id'           => 'integer',
        'ip'                 => 'string',
        'platform'           => 'string',
        'user_id'            => 'integer',
        'from_created_at'    => 'string',
        'to_created_at'      => 'string',
    ];


    /**
     * Registered map filters to operate upon.
     *
     * @var array
     */
    protected array $mapFilters = [
        'user_id'           => 'fk_user_id',
        'from_created_at'   => 'created_at',
        'to_created_at'     => 'created_at',
    ];

    /**
     * @param string $fromCreatedAt FromCreatedAt.
     *
     * @return Builder
     */
    protected function fromCreatedAt(string $fromCreatedAt): Builder
    {
        return $this->builder->whereFromCreatedAt($fromCreatedAt);
    }

    /**
     * @param string $toCreatedAt ToCreatedAt.
     *
     * @return Builder
     */
    protected function toCreatedAt(string $toCreatedAt): Builder
    {
        return $this->builder->whereToCreatedAt($toCreatedAt);
    }
}
