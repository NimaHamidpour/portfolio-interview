<?php

namespace App\Filters;

use App\Enum\Tag;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;

class TagFilter extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected array $filters = [
        Tag::ID                                   => '=',
        Tag::COLOR                                => 'like',
        Tag::IS_ACTIVE                            => '=',
        Tag::CREATED_AT                           => '=',
        Tag::FROM_CREATED_AT                      => '>=',
        Tag::TO_CREATED_AT                        => '<=',
    ];

    /**
     * Registered filters type to operate upon.
     *
     * @var array
     */
    protected array $filtersType = [
        Tag::ID                                   => Tag::INTEGER,
        Tag::NAME                                 => Tag::STRING,
        Tag::COLOR                                => Tag::STRING,
        Tag::IS_ACTIVE                            => Tag::BOOLEAN,
        Tag::CREATED_AT                           => Tag::STRING,
        Tag::FROM_CREATED_AT                      => Tag::STRING,
        Tag::TO_CREATED_AT                        => Tag::STRING,
    ];

    /**
     * Registered custom filters to operate upon.
     *
     * @var array
     */
    protected array $customFilters = [
        'name',
    ];

    /**
     * Registered map filters to operate upon.
     *
     * @var array
     */
    protected array $mapFilters = [
        Tag::FROM_CREATED_AT          => Tag::CREATED_AT,
        Tag::TO_CREATED_AT            => Tag::CREATED_AT,
    ];

    /**
     * @param string $name Name.
     * @return Builder
     */
    protected function name(string $name): Builder
    {
        return $this->builder->containing($name);
    }
}
