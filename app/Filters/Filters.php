<?php

namespace App\Filters;

use App\Traits\ConvertDateTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Morilog\Jalali\Jalalian;

abstract class Filters
{
    use ConvertDateTrait;

    /**
     * @var Request
     */
    protected Request $request;

    /**
     * The Eloquent builder.
     *
     * @var Builder
     */
    protected Builder $builder;

    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected array $filters = [];

    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected array $customFilters = [];

    /**
     * @var array
     */
    protected array $filtersType = [];

    /**
     * @var array
     */
    protected array $mapFilters = [];

    /**
     * @var array
     */
    protected array $jalaliFilters = [];

    /**
     * Create a new ThreadFilters instance.
     *
     * @param Request $request BaseRequest.
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply the filters.
     *
     * @param Builder $builder Builder.
     * @return Builder
     */
    public function apply(Builder $builder): Builder
    {
        $this->builder = $builder;

        foreach ($this->getFilters() as $filter => $value) {
            $field = $this->setField($filter);
            $operator = $this->filters[$filter];
            $value = $this->setValue($operator, $value);

            $this->findByField($field, $operator, $value);
        }

        foreach ($this->getCustomFilters() as $customFilter => $value) {
            $method = $this->mapFilters[$customFilter] ?? $customFilter;

            if (method_exists($this, $method)) {
                $type = $this->filtersType[$customFilter];
                settype($value, $type);
                $this->$method($value);
            }
        }

        if ($this->request->filled('orderBy')) {
            $this->orderBy($this->request->orderBy);
        }

        return $this->builder;
    }

    /**
     * Fetch all relevant filters from the request.
     *
     * @return array
     */
    public function getFilters()
    {
        return array_filter($this->request->only(array_keys($this->filters)), function ($item) {
            return !is_null($item);
        });
    }

    /**
     * Fetch all relevant filters from the request.
     *
     * @return array
     */
    public function getCustomFilters()
    {
        return array_filter($this->request->only($this->customFilters), function ($item) {
            return !is_null($item);
        });
    }

    /**
     * @param string $filter Filter.
     * @return mixed
     */
    protected function setField(string $filter)
    {
        return $this->mapFilters[$filter] ?? $filter;
    }

    /**
     * @param string $operator Operator.
     * @param mixed  $value    Value.
     * @return mixed
     */
    protected function setValue(string $operator, mixed $value)
    {
        if ($operator == 'like') {
            return '%' . $value . '%';
        }

        return $value;
    }

    /**
     * Order the query by givens orders
     *
     * @param array|string $orders Orders.
     *
     * @return Builder
     */
    protected function orderBy($orders)
    {
        if (! is_array($orders)) {
            $orders = json_decode($orders, true);
        }

        return $this->builder->when(! empty($orders), function ($query) use ($orders) {
            foreach ($orders as $key => $order) {
                $query->orderBy($key, $order);
            }
        });
    }

    /**
     * @param string $field    Field.
     * @param string $operator Operator.
     * @param mixed  $value    Value.
     * @return Builder
     */
    protected function findByField(string $field, string $operator, mixed $value): Builder
    {
        return $this->builder->where($field, $operator, $value);
    }
}
