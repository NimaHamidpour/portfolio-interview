<?php

namespace App\Filters;

use App\Enum\Category;

class CategoryFilter extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected array $filters = [
        Category::CATEGORY_ID                  => '=',
        Category::NAME                         => 'like',
        Category::SLUG                         => 'like',
        Category::IS_ACTIVE                    => '=',
        Category::CREATED_AT                   => '=',
        Category::PARENT                       => '=',
        'from_created_at'                      => '>=',
        'to_created_at'                        => '<=',
    ];

    /**
     * Registered filters type to operate upon.
     *
     * @var array
     */
    protected array $filtersType = [
        Category::CATEGORY_ID                  => 'integer',
        Category::NAME                         => 'string',
        Category::SLUG                         => 'string',
        Category::IS_ACTIVE                    => 'integer',
        Category::CREATED_AT                   => 'string',
        Category::PARENT                       => 'integer',
        'from_created_at'                      => 'string',
        'to_created_at'                        => 'string',
    ];

    /**
     * Registered map filters to operate upon.
     *
     * @var array
     */
    protected array $mapFilters = [
        Category::PARENT           => Category::PARENT_ID,
        'from_created_at'          => Category::CREATED_AT,
        'to_created_at'            => Category::CREATED_AT,
    ];
}
