<?php

use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

if (!function_exists('checkTTl')) {
    /**
     * @param string $key Key.
     * @return integer
     */
    function checkTtl(string $key): int
    {
        $ttl = Redis::ttl($key);
        abort_if($ttl > 0, Response::HTTP_FORBIDDEN, __('auth.otp_ttl' . $ttl));

        return $ttl;
    }
}

if (!function_exists('makeAllRouteName')) {
    /**
     * @param string $routeName Get route name.
     * @return string
     */
    function makeAllRouteName(string $routeName): string
    {
        $routeNameSections = explode('.', $routeName);
        return $routeNameSections[0] == 'api'
            ? sprintf(
                '%s.%s.%s.%s',
                $routeNameSections[0],
                $routeNameSections[1],
                $routeNameSections[2],
                '*'
            ) : sprintf(
                '%s.%s.%s.%s.%s',
                $routeNameSections[0],
                $routeNameSections[1],
                $routeNameSections[2],
                $routeNameSections[3],
                '*'
            );
    }
}

if (!function_exists('logAllRequestQueries')) {
    /**
     * @return void
     */
    function logAllRequestQueries(): void
    {
        DB::listen(function ($query) {
            Log::info(
                $query->sql,
                $query->bindings,
            );
        });
    }
}
