<?php

namespace App\Enum;

class Luck extends BaseEnum
{
    const TABLE = 'lucks';
    const LUCK_ID = 'luck_id';
    const FK_BUSINESS_ID = 'fk_business_id';
    const TITLE = 'title';
}
