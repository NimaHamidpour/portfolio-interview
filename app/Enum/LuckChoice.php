<?php

namespace App\Enum;

class LuckChoice extends BaseEnum
{
    const TABLE = 'luck_choices';
    const LUCK_CHOICE_ID = 'luck_choice_id';
    const FK_LUCK_ID = 'fk_luck_id';
    const TYPE = 'type';
    const VALUE = 'value';

    const PRODUCT = 'product';
    const CODE = 'product';
    const EMPTY = 'empty';
    /**
     * @var array|string[]
     */
    public static array $luckChoiceType = [
        self::PRODUCT,
        self::CODE,
        self::EMPTY,
    ];
}
