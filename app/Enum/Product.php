<?php

namespace App\Enum;

class Product extends BaseEnum
{
    const TABLE = 'products';
    const PRODUCT_ID = 'product_id';
    const SKU = 'sku';
    const FK_BUSINESS_ID = 'fk_business_id';
    const FK_CATEGORY_ID = 'fk_category_id';
    const BRAND = 'brand';
    const QUANTITY = 'quantity';
    const PRICE = 'price';
    const DISCOUNT_PERCENT = 'discount_price';
    const DESCRIPTION = 'description';
    const DETAIL = 'detail';
    const COLOR = 'color';
    const ALLOW_COMMENT = 'allow_comment';
}
