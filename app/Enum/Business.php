<?php

namespace App\Enum;

class Business extends BaseEnum
{
    const TABLE = 'businesses';
    const BUSINESS_ID = 'business_id';
    const FK_USER_ID = 'fk_user_id';
    const FK_CATEGORY_ID = 'fk_category_id';
    const TITLE = 'title';
    const SLUG = 'slug';
    const DESCRIPTION = 'description';
    const FK_CITY_ID = 'fk_city_id';
    const REGION = 'region';
    const MAP = 'map';
    const MOBILE = 'mobile';
    const PHONE = 'phone';
    const WEBSITE = 'website';
    const WHATSAPP = 'whatsapp';
    const TELEGRAM = 'telegram';
    const INSTAGRAM = 'instagram';
    const EMAIL = 'email';
    const WALLET = 'wallet';
    const ALLOW_CHARGE = 'allow_charge';
    const LOCK_WITHDRAW = 'lock_withdraw';
    const LOCK_BALANCE = 'lock_balance';
    const STATUS = 'status';
    const ACTIVE = 'active';
    const DE_ACTIVE = 'de_active';
    const PENDING = 'pending';
    /**
     * @var array|string[]
     */
    public static array $statusTYpe = [
        self::ACTIVE,
        self::DE_ACTIVE,
        self::PENDING,
    ];
}
