<?php

namespace App\Enum;

class UserBank extends BaseEnum
{
    const TABLE = 'user_banks';
    const USER_BANK_ID = 'user_bank_id';
    const FK_USER_ID = 'fk_user_id';
    const BANK_NAME = 'bank_name';
    const SHEBA = 'sheba';
    const ACCOUNT_NUMBER = 'account_number';
    const CARD_NUMBER = 'card_number';
    const OWNER_NAME = 'owner_name';
    const BANK = 'bank';
    /**
     * @var array|string[]
     */
    public static array $validCardNumber = [
        '6037997211742706',
        '6037697541683998',
        '5859831191658524',
        '5022291302858848'
    ];

    /**
     * @var array|string[]
     */
    public static array $inValidCardNumber = [
        '6037997211742705',
        '6037697541683997',
        '5859831191658523',
        '5022291302858842'
    ];
}
