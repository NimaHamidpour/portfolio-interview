<?php

namespace App\Enum;

class Role extends BaseEnum
{
    const SUPER_ADMIN = 'SUPERADMIN';
    const ADMIN = 'ADMIN';
    const OPERATOR = 'OPERATOR';
    const USER = 'USER';

    /**
     * @var string[]
     */
    public static array $superAdminPermissions = [];

    /**
     * @return array
     */
    public static function getSuperAdminPermissions(): array
    {
        return array_merge(self::$adminPermissions, self::$superAdminPermissions);
    }

    /**
     * @return string[]
     */
    public static function getAllRoles(): array
    {
        return [
            self::SUPER_ADMIN,
            self::ADMIN,
            self::OPERATOR,
            self::USER
        ];
    }
}
