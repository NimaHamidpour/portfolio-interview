<?php

namespace App\Enum;

class Tag extends BaseEnum
{
    const TABLE = 'tags';
    const TAGGABLES_TABLE = 'taggables';
    const ID = 'id';
    const NAME = 'name';
    const SLUG = 'slug';
    const TYPE = 'type';
    const ORDER_COLUMN = 'order_column';
    const COLOR = 'color';
    const IS_ACTIVE = 'is_active';
}
