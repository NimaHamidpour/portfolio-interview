<?php

namespace App\Enum;

class User extends BaseEnum
{
    const TABLE = 'users';
    const USER_ID = 'user_id';
    const AVATAR = 'avatar';
    const USERNAME = 'username';
    const FIRST_NAME = 'first_name';
    const LAST_NAME = 'last_name';
    const NATIONAL_CODE = 'national_code';
    const EMAIL = 'email';
    const MOBILE = 'mobile';
    const PASSWORD = 'password';
    const REGISTER_IP = 'register_ip';
    const BIRTH_DATE = 'birthdate';
    const PHONE = 'phone';
    const GENDER = 'gender';
    const PROVINCE = 'province';
    const ADDRESS = 'address';
    const ZIPCODE = 'zipcode';
    const STATUS = 'status';
    const AFFILIATE_CODE = 'affiliate_code';
    const ALLOW_CHARGE = 'allow_charge';
    const LOCK_BALANCE = 'lock_balance';
    const LOCK_WITHDRAW = 'lock_withdraw';
    const OTP = 'otp';

    const MALE = 'male';
    const FEMALE = 'female';

    /**
     * @var string[] User Types.
     */
    public static array $genderTypes = [
        self::MALE,
        self::FEMALE,
    ];
}
