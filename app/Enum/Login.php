<?php

namespace App\Enum;

class Login extends BaseEnum
{
    const TABLE = 'logins';
    const LOGIN_ID = 'login_id';
    const IP = 'ip';
    const FK_USER_ID = 'fk_user_id';
    const USER = 'user';
    const PLATFORM = 'platform';
    const ANDROID = 'android';
    const IOS = 'ios';
    const WEB = 'web';

    /**
     * @var string[] platform.
     */
    public static array $platform = [
        self::ANDROID,
        self::IOS,
        self::WEB
    ];
}
