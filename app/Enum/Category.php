<?php

namespace App\Enum;

class Category extends BaseEnum
{
    const TABLE = 'categories';
    const CATEGORY_ID = 'category_id';
    const SLUG = 'slug';
    const NAME = 'name';
    const PRIORITY = 'priority';
    const IS_ACTIVE = 'is_active';
    const PARENT_ID = 'parent_id';
    const PARENT = 'parent';
    const LEFT = '_lft';
    const RIGHT = '_rgt';
}
