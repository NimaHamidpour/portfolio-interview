<?php

namespace App\Enum;

class Permission extends BaseEnum
{
    const GET_ALL_USERS = 'GetAllUsers';
    const GET_USER = 'GetUser';
    const CREATE_USER = 'CreateUser';
    const UPDATE_USER = 'UpdateUser';
    const DELETE_USER = 'DeleteUser';
}
