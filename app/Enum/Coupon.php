<?php

namespace App\Enum;

class Coupon extends BaseEnum
{
    const TABLE = 'coupons';

    const COUPON_ID = 'coupon_id';
    const FK_BUSINESS_ID = 'fk_business_id';
    const TITLE = 'title';
    const CODE = 'code';
    const PERCENT = 'percent';
    const QUANTITY = 'quantity';
    const USED = 'used';
    const EXPIRED_AT = 'expired_at';
}
