<?php

namespace App\Enum;

class Config extends BaseEnum
{
    const TABLE = 'configs';
    const ID = 'config_id';
    const KEY = 'key';
    const VALUE = 'value';
    const GROUP = 'group';

    const USER_UPLOAD_FILE = 'user_upload_file';

    /**
     * @var string[] configGroups.
     */
    public static array $configGroups = [

        self::USER_UPLOAD_FILE,
    ];

    /**
     * @var string[] clientConfig.
     */
    public static array $clientConfig = [
        self::USER_UPLOAD_FILE,
    ];
}
