<?php

namespace App\Extra\MediaLibrary;

use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\Support\PathGenerator\PathGenerator;

class CustomPathGenerator implements PathGenerator
{

    /**
     * Return media path.
     *
     * @param Media $media Media.
     * @return string
     */
    public function getPath(Media $media): string
    {
        return $media->created_at->format('Y-m-d') . DIRECTORY_SEPARATOR . md5($media->id . $media->file_name . $media->media_types) . DIRECTORY_SEPARATOR;
    }

    /**
     * Return media path.
     *
     * @param Media $media Media.
     * @return string
     */
    public function getPathForConversions(Media $media): string
    {
        return $this->getPath($media) . 'x' . DIRECTORY_SEPARATOR;
    }

    /**
     * Return media path.
     *
     * @param Media $media Media.
     * @return string
     */
    public function getPathForResponsiveImages(Media $media): string
    {
        return $this->getPath($media) . '/cri/';
    }
}
