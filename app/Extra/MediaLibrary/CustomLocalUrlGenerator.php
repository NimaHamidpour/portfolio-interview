<?php

namespace App\Extra\MediaLibrary;

use DateTimeInterface;
use Spatie\MediaLibrary\Conversions\Conversion;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\Support\PathGenerator\PathGenerator;
use Spatie\MediaLibrary\Support\UrlGenerator\BaseUrlGenerator;

class CustomLocalUrlGenerator extends BaseUrlGenerator
{
    /**
     * @return string
     */
    public function getUrl(): string
    {
        $url = $this->getBaseMediaDirectoryUrl() . '/' . $this->getPathRelativeToRoot();

        $url = $this->makeCompatibleForNonUnixHosts($url);

        $url = $this->rawUrlEncodeFilename($url);

        return $url;
    }

    /**
     * @return string
     */
    protected function getBaseMediaDirectoryUrl(): string
    {
        if ($diskUrl = $this->config->get("filesystems.disks.{$this->media->disk}.url")) {
            return str_replace(url('/'), '', $diskUrl);
        }

        return $this->getBaseMediaDirectory();
    }

    /**
     * @return string
     */
    protected function getBaseMediaDirectory(): string
    {
        return str_replace(public_path(), '', $this->getStoragePath());
    }

    /**
     * @return string
     */
    protected function getStoragePath(): string
    {
        $diskRootPath = $this->config->get("filesystems.disks.{$this->media->disk}.root");

        return realpath($diskRootPath);
    }

    /**
     * makeCompatibleForNonUnixHosts.
     *
     * @param string $url String.
     * @return string
     */
    protected function makeCompatibleForNonUnixHosts(string $url): string
    {
        if (DIRECTORY_SEPARATOR != '/') {
            $url = str_replace(DIRECTORY_SEPARATOR, '/', $url);
        }

        return $url;
    }

    /**
     * rawUrlEncodeFilename.
     *
     * @param string $path String.
     * @return string
     */
    public function rawUrlEncodeFilename(string $path = ''): string
    {
        return pathinfo($path, PATHINFO_DIRNAME) . '/' . rawurlencode($this->media->file_name);
    }

    /**
     * @param Media $media Media.
     * @return $this
     */
    public function setMedia(Media $media): self
    {
        $this->media = $media;

        return $this;
    }

    /**
     * @param PathGenerator $pathGenerator PathGenerator.
     * @return $this
     */
    public function setPathGenerator(PathGenerator $pathGenerator): self
    {
        $this->pathGenerator = $pathGenerator;

        return $this;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        // TODO: Implement getPath() method.
    }

    /**
     * @param Conversion $conversion
     * @return $this
     */
    public function setConversion(Conversion $conversion): self
    {
        // TODO: Implement setConversion() method.
    }

    /**
     * @param DateTimeInterface $expiration
     * @param array $options
     * @return string
     */
    public function getTemporaryUrl(DateTimeInterface $expiration, array $options = []): string
    {
        // TODO: Implement getTemporaryUrl() method.
    }

    /**
     * @return string string.
     */
    public function getResponsiveImagesDirectoryUrl(): string
    {
        // TODO: Implement getResponsiveImagesDirectoryUrl() method.
    }
}
