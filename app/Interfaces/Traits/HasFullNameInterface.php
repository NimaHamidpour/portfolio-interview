<?php

namespace App\Interfaces\Traits;

use Illuminate\Database\Eloquent\Builder;

interface HasFullNameInterface
{
    /**
     * @param Builder $builder  Builder.
     * @param string  $fullName Full Name.
     *
     * @return Builder
     */
    public function scopeWhereFullNameLike(Builder $builder, string $fullName): Builder;
}
