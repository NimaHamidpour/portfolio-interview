<?php

namespace App\Interfaces\Models;

use App\Filters\CategoryFilter;
use Illuminate\Database\Eloquent\Builder;

interface CategoryInterface
{
    /**
     * Filter scope.
     *
     * @param Builder        $builder Builder.
     * @param CategoryFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, CategoryFilter $filters): Builder;
}
