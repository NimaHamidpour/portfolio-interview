<?php

namespace App\Interfaces\Models;

use App\Filters\TagFilter;
use Illuminate\Database\Eloquent\Builder;

interface TagInterface
{
    /**
     * Filter scope.
     *
     * @param Builder   $builder Builder.
     * @param TagFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, TagFilter $filters): Builder;
}
