<?php

namespace App\Interfaces\Models;

use App\Filters\UserFilter;
use App\Interfaces\Traits\HasFullNameInterface;
use Illuminate\Database\Eloquent\Builder;

interface UserInterface extends
    HasFullNameInterface
{
    /**
     * Filter scope.
     *
     * @param Builder    $builder Builder.
     * @param UserFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, UserFilter $filters): Builder;
}
