<?php

namespace App\Interfaces\Models;

use App\Filters\UserBankFilter;
use Illuminate\Database\Eloquent\Builder;

interface UserBankInterface
{
    /**
     * Filter scope.
     *
     * @param Builder        $builder Builder.
     * @param UserBankFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, UserBankFilter $filters): Builder;
}
