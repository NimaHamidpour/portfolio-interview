<?php

namespace App\Interfaces\Models;

use App\Filters\VerificationFilter;
use Illuminate\Database\Eloquent\Builder;

interface VerificationInterface
{
    /**
     * Filter scope.
     *
     * @param Builder            $builder Builder.
     * @param VerificationFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, VerificationFilter $filters): Builder;
}
