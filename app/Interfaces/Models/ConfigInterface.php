<?php

namespace App\Interfaces\Models;

use App\Filters\ConfigFilter;
use Illuminate\Database\Eloquent\Builder;

interface ConfigInterface
{
    /**
     * Filter scope.
     *
     * @param Builder      $builder Builder.
     * @param ConfigFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, ConfigFilter $filters): Builder;


    /**
     * Create new config.
     *
     * @param string      $key   Key.
     * @param string      $value Value.
     * @param string|null $group Group.
     * @return ConfigInterface
     */
    public static function createObject(
        string $key,
        string $value,
        ?string $group = null
    ): ConfigInterface;


    /**
     * Update new config.
     *
     * @param string      $key   Key.
     * @param string      $value Value.
     * @param string|null $group Group.
     * @return ConfigInterface
     */
    public function updateObject(
        string $key,
        string $value,
        ?string $group = null
    ): ConfigInterface;
}
