<?php

namespace App\Interfaces\Models;

use App\Filters\BusinessFilter;
use App\Filters\CategoryFilter;
use Illuminate\Database\Eloquent\Builder;

interface BusinessInterface
{
    /**
     * Filter scope.
     *
     * @param Builder        $builder Builder.
     * @param BusinessFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, BusinessFilter $filters): Builder;
}
