<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class OdinException extends Exception
{
    protected array $error;

    public function __construct(int $httpCode = Response::HTTP_BAD_REQUEST, string $message = '', array $error = [])
    {
        $message = empty($message) ? trans('messages.response.bad_request') : $message;
        $this->error = $error;
        parent::__construct(message: $message, code: $httpCode);
    }

    /**
     * @return array
     */
    public function getError() : array {
        return $this->error;
    }
}
