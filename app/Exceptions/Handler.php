<?php

namespace App\Exceptions;

use App\Traits\ResponseTrait;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Validation\UnauthorizedException;
use Spatie\Permission\Exceptions\UnauthorizedException as SpatieUnauthorizedException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    use ResponseTrait;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $e)
    {
        $error = [];
        switch (true) {
            case $e instanceof NotFoundHttpException:
                $message = !empty($e->getMessage()) ? $e->getMessage() : trans('messages.response.not_found');
                $httpCode = Response::HTTP_NOT_FOUND;
                break;

            case $e instanceof SpatieUnauthorizedException:
                $message = trans('messages.response.forbidden');
                $httpCode = Response::HTTP_FORBIDDEN;
                break;

            case $e instanceof ModelNotFoundException:
                $message = trans('messages.response.not_found');
                $httpCode = Response::HTTP_NOT_FOUND;
                break;

            case $e instanceof UnauthorizedException:
            case $e instanceof AuthenticationException:
                $message = trans('messages.response.unauthorized');
                $httpCode = Response::HTTP_UNAUTHORIZED;
                break;

            case $e instanceof ThrottleRequestsException:
                $message = !empty($e->getMessage()) ? $e->getMessage() : trans('auth.throttle');
                $httpCode = Response::HTTP_TOO_MANY_REQUESTS;
                break;

            case $e instanceof AuthorizationException:
            case $e instanceof HttpException && $e->getStatusCode() == Response::HTTP_FORBIDDEN:
                $message = !empty($e->getMessage()) ? $e->getMessage() : trans('messages.response.forbidden');
                $httpCode = Response::HTTP_FORBIDDEN;
                break;

            case $e instanceof OdinException:
                $message = $e->getMessage();
                $httpCode = $e->getCode();
                $error = $e->getError();
                break;

            default:
                $message = !empty($e->getMessage()) ? $e->getMessage() : trans('messages.response.bad_request');
                $httpCode = Response::HTTP_BAD_REQUEST;
        }

        return $this->sendError($error, $httpCode, $message);
    }
}
