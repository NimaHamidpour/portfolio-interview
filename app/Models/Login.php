<?php

namespace App\Models;

use App\Enum\Login as EnumLogin;
use App\Filters\LoginFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Login extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        EnumLogin::IP,
        EnumLogin::FK_USER_ID,
        EnumLogin::PLATFORM
    ];

    /**
     * @var string
     */
    protected $primaryKey = EnumLogin::LOGIN_ID;

    /**
     * Filter scope.
     *
     * @param Builder     $builder Builder.
     * @param LoginFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, LoginFilter $filters): Builder
    {
        return $filters->apply($builder);
    }


    /**
     * Create new login.
     *
     * @param integer     $fk_user_id Fk_user_id.
     * @param string      $ip         Ip.
     * @param string|null $platform   Platform.
     *
     * @return LoginInterface
     */
    public static function createObject(
        int $fk_user_id,
        string $ip,
        ?string $platform = null,
    ): LoginInterface {
        $login = new static();
        $login->fk_user_id = $fk_user_id;
        $login->ip = $ip;
        if ($platform != null) {
            $login->platform = $platform;
        }


        $login->save();

        return $login;
    }


    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'fk_user_id', 'user_id');
    }
}
