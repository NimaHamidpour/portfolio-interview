<?php

namespace App\Models;

use App\Enum\Luck as EnumLuck;
use App\Enum\LuckChoice as EnumLuckChoice;
use App\Models\Business;
use App\Enum\Business as EnumBusiness;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Luck extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    protected $fillable = [
        EnumLuck::LUCK_ID,
        EnumLuck::FK_BUSINESS_ID,
        EnumLuck::TITLE
    ];

    /**
     * @return BelongsTo
     */
    public function business()
    {
        return $this->belongsTo(Business::class, EnumBusiness::BUSINESS_ID, EnumLuck::FK_BUSINESS_ID);
    }


    /**
     * @return HasMany
     */
    public function luckChoice()
    {
        return $this->hasMany(LuckChoice::class, EnumLuckChoice::FK_LUCK_ID, EnumLuck::LUCK_ID);
    }
}
