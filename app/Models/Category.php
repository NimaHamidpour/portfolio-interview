<?php

namespace App\Models;

use App\Enum\Category as EnumCategory;
use App\Filters\CategoryFilter;
use App\Interfaces\Models\CategoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kalnoy\Nestedset\NodeTrait;

class Category extends Model implements CategoryInterface
{
    use HasFactory;
    use SoftDeletes;
    use NodeTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        EnumCategory::SLUG,
        EnumCategory::NAME,
        EnumCategory::IS_ACTIVE,
        EnumCategory::PRIORITY,
    ];

    /**
     * @var string
     */
    protected $primaryKey = EnumCategory::CATEGORY_ID;

    /**
     * Filter scope.
     *
     * @param Builder        $builder Builder.
     * @param CategoryFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, CategoryFilter $filters): Builder
    {
        return $filters->apply($builder);
    }
}
