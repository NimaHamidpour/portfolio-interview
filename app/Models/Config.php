<?php

namespace App\Models;

use App\Enum\Config as EnumConfig;
use App\Filters\ConfigFilter;
use App\Interfaces\Models\ConfigInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Config extends Model implements ConfigInterface
{
    use HasFactory;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        EnumConfig::KEY,
        EnumConfig::VALUE,
        EnumConfig::GROUP,
    ];

    /**
     * @var string
     */
    protected $primaryKey = EnumConfig::ID;

    /**
     * @param Builder      $builder Builder.
     * @param ConfigFilter $filters Filter.
     * @return Builder
     */
    public function scopeFilter(Builder $builder, ConfigFilter $filters): Builder
    {
        return $filters->apply($builder);
    }

    /**
     * create new config.
     * @param string      $key   Key.
     * @param string      $value Value.
     * @param string|null $group Group.
     * @return ConfigInterface
     */
    public static function createObject(string $key, string $value, ?string $group = null): ConfigInterface
    {
        $config = new static();
        $config->key = $key;
        $config->value = $value;
        $config->group = $group;
        $config->save();

        return $config;
    }

    /**
     * update existing config.
     * @param string      $key   Key.
     * @param string      $value Value.
     * @param string|null $group Group.
     * @return self
     */
    public function updateObject(string $key, string $value, ?string $group = null): ConfigInterface
    {
        $this->key = $key;
        $this->value = $value;
        $this->group = $group;
        $this->save();

        return $this;
    }
}
