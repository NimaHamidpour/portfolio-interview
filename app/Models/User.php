<?php

namespace App\Models;

use App\Enum\User as EnumUser;
use App\Enum\UserBank as EnumUserBank;
use App\Filters\UserFilter;
use App\Interfaces\Models\UserInterface;
use App\Lib\CacheManager\CacheManager;
use App\Traits\Scopes\HasFullNameTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use App\Enum\User as Enum;
use Morilog\Jalali\Jalalian;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements UserInterface, JWTSubject
{
    use HasFactory;
    use Notifiable;
    use Notifiable;
    use HasFullNameTrait;
    use HasRoles;
    use HasRoles {
        hasRole as traitHasRole;
    }

    /**
      * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        Enum::AVATAR,
        Enum::USERNAME,
        Enum::FIRST_NAME,
        Enum::LAST_NAME,
        Enum::MOBILE,
        Enum::NATIONAL_CODE,
        Enum::EMAIL,
        Enum::PHONE,
        Enum::ZIPCODE,
        Enum::ADDRESS,
        Enum::BIRTH_DATE,
        Enum::REGISTER_IP,
        Enum::STATUS,
        Enum::ALLOW_CHARGE,
        Enum::AFFILIATE_CODE,
        Enum::LOCK_WITHDRAW,
        Enum::LOCK_BALANCE,
        Enum::PASSWORD,
    ];

    /**
     * @var string
     */
    protected $primaryKey = Enum::USER_ID;

    /**
     * @param array|Role|string $roles Get roles.
     * @return boolean
     */
    public function hasRole($roles): bool
    {
        $authCacheEnabled = config('cache.authorization_cache_enabled');
        $authCacheTtl = config('cache.authorization_cache_ttl');

        if ((empty($key = $this->getRoleCacheKey($roles))) || !$authCacheEnabled) {
            return $this->traitHasRole($roles);
        }

        return CacheManager::getFromCache($key, function () use ($roles) {
            return $this->traitHasRole($roles);
        }, $authCacheTtl);
    }

    /**
     * @param array|Role|string $roles Get roles.
     * @return boolean
     */
    public function hasAnyRole(array $roles): bool
    {
        if (is_string($roles) && false !== strpos($roles, '|')) {
            $roles = $this->convertPipeToArray($roles);
        }

        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param string|Role|array $role Get role.
     * @return string|null
     */
    private function getRoleCacheKey($role): ?string
    {
        if (is_string($role)) {
            $key = $role;
        }

        if ($role instanceof Role) {
            $key = $role->toJson();
        }

        if (is_array($role)) {
            $key = json_encode($role);
        }

        return isset($key) ?
            sprintf('%s:%s:%s:%s', 'user', $this[$this->primaryKey], 'roles', md5($key)) : null;
    }

    /**
     * Filter scope.
     *
     * @param Builder    $builder Builder.
     * @param UserFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, UserFilter $filters): Builder
    {
        return $filters->apply($builder);
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        Enum::PASSWORD,
    ];
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier(): mixed
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }
    /**
     * @return HasMany
     */
    public function bankInfo(): HasMany
    {
        return $this->hasMany(UserBank::class, EnumUserBank::FK_USER_ID, EnumUser::USER_ID);
    }


    /**
     * @return string
     */
    public function getFullNameAttribute(): string
    {
        return sprintf('%s %s', $this->first_name, $this->last_name);
    }
}
