<?php

namespace App\Models;

use App\Enum\LuckChoice as EnumLuckChoice;
use App\Enum\Luck as EnumLuck;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class LuckChoice extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    protected $fillable = [
      EnumLuckChoice::LUCK_CHOICE_ID,
      EnumLuckChoice::FK_LUCK_ID,
      EnumLuckChoice::TYPE,
      EnumLuckChoice::VALUE
    ];


    /**
     * @return BelongsTo
     */
    public function luck()
    {
        return $this->belongsTo(Luck::class, EnumLuck::FK_BUSINESS_ID, EnumLuckChoice::FK_LUCK_ID);
    }
}
