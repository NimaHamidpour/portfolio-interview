<?php

namespace App\Models;

use App\Enum\Business as EnumBusiness;
use App\Enum\Category;
use App\Enum\Product as EnumProduct;
use App\Enum\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Product extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    protected $fillable = [
        EnumProduct::PRODUCT_ID,
        EnumProduct::SKU,
        EnumProduct::FK_BUSINESS_ID,
        EnumProduct::FK_CATEGORY_ID,
        EnumProduct::BRAND,
        EnumProduct::QUANTITY,
        EnumProduct::PRICE,
        EnumProduct::DISCOUNT_PERCENT,
        EnumProduct::DESCRIPTION,
        EnumProduct::DETAIL,
        EnumProduct::COLOR,
        EnumProduct::ALLOW_COMMENT,
    ];

    /**
     * @return BelongsTo
     */
    public function business()
    {
        return $this->belongsTo(Business::class, EnumProduct::FK_BUSINESS_ID, EnumBusiness::BUSINESS_ID);
    }

    /**
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, EnumProduct::FK_CATEGORY_ID, Category::CATEGORY_ID);
    }
}
