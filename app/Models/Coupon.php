<?php

namespace App\Models;

use App\Enum\Business as EnumBusiness;
use App\Enum\Coupon as EnumCoupon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Coupon extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    protected $fillable = [
        EnumCoupon::COUPON_ID,
        EnumCoupon::FK_BUSINESS_ID,
        EnumCoupon::TITLE,
        EnumCoupon::CODE,
        EnumCoupon::PERCENT,
        EnumCoupon::QUANTITY,
        EnumCoupon::USED,
        EnumCoupon::EXPIRED_AT,
    ];

    /**
     * @return BelongsTo
     */
    public function business()
    {
        return $this->belongsTo(Business::class, EnumBusiness::BUSINESS_ID, EnumCoupon::FK_BUSINESS_ID);
    }
}
