<?php

namespace App\Models;

use App\Enum\Business as EnumBusiness;
use App\Models\User;
use App\Enum\User as EnumUser;
use App\Models\Category;
use App\Models\Product;
use App\Enum\Category as EnumCategory;
use App\Enum\Product as EnumProduct;
use App\Enum\Luck as EnumLuck;
use App\Filters\BusinessFilter;
use App\Filters\CategoryFilter;
use App\Interfaces\Models\BusinessInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Business extends Model implements BusinessInterface
{
    use HasFactory;
    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        EnumBusiness::BUSINESS_ID,
        EnumBusiness::FK_USER_ID,
        EnumBusiness::FK_CATEGORY_ID,
        EnumBusiness::TITLE,
        EnumBusiness::SLUG,
        EnumBusiness::DESCRIPTION,
        EnumBusiness::FK_CITY_ID,
        EnumBusiness::REGION,
        EnumBusiness::MAP,
        EnumBusiness::MOBILE,
        EnumBusiness::PHONE,
        EnumBusiness::WEBSITE,
        EnumBusiness::WHATSAPP,
        EnumBusiness::TELEGRAM,
        EnumBusiness::INSTAGRAM,
        EnumBusiness::EMAIL,
        EnumBusiness::WALLET,
        EnumBusiness::ALLOW_CHARGE,
        EnumBusiness::LOCK_WITHDRAW,
        EnumBusiness::LOCK_BALANCE,
        EnumBusiness::STATUS,
    ];

    /**
     * @var string
     */
    protected $primaryKey = EnumBusiness::BUSINESS_ID;
    /**
     * Filter scope.
     *
     * @param Builder        $builder Builder.
     * @param BusinessFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, BusinessFilter $filters): Builder
    {
        return $filters->apply($builder);
    }

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, EnumBusiness::FK_USER_ID, EnumUser::USER_ID);
    }

    /**
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, EnumBusiness::FK_CATEGORY_ID, EnumCategory::CATEGORY_ID);
    }
    /**
     * @return HasMany
     */
    public function product()
    {
        return $this->hasMany(Product::class, EnumProduct::FK_BUSINESS_ID, EnumBusiness::BUSINESS_ID);
    }
    /**
     * @return HasMany
     */
    public function luck()
    {
        return $this->hasMany(Luck::class, EnumLuck::FK_BUSINESS_ID, EnumBusiness::BUSINESS_ID);
    }
}
