<?php

namespace App\Models;

use App\Enum\UserBank as EnumUserBank;
use App\Filters\UserBankFilter;
use App\Interfaces\Models\UserBankInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class UserBank extends Model implements UserBankInterface
{
    use HasFactory;

    /**
     * @var string
     */
    protected $primaryKey = EnumUserBank::USER_BANK_ID;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      EnumUserBank::FK_USER_ID,
      EnumUserBank::BANK_NAME,
      EnumUserBank::SHEBA,
      EnumUserBank::OWNER_NAME,
      EnumUserBank::CARD_NUMBER,
    ];

    /**
     * Filter scope.
     *
     * @param Builder        $builder Builder.
     * @param UserBankFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, UserBankFilter $filters): Builder
    {
        return $filters->apply($builder);
    }


    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'fk_user_id', 'user_id');
    }
}
