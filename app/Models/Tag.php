<?php

namespace App\Models;

use App\Filters\TagFilter;
use App\Interfaces\Models\TagInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Tags\Tag as SpatieTag;

class Tag extends SpatieTag implements TagInterface
{
    use HasFactory;
    use SoftDeletes;

    /**
     * Filter scope.
     *
     * @param Builder   $builder Builder.
     * @param TagFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, TagFilter $filters): Builder
    {
        return $filters->apply($builder);
    }
}
