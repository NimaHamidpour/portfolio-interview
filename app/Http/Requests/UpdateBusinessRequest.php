<?php

namespace App\Http\Requests;

use App\Enum\Business;
use App\Enum\User;
use Illuminate\Validation\Rule;

class UpdateBusinessRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            Business::FK_CATEGORY_ID => ['required','exists:categories,category_id'],
            Business::FK_CITY_ID => ['required'],
            Business::TITLE   => ['required','string'],
            Business::SLUG    => ['required','string',Rule::unique('businesses')->ignore($this->route('business'))],
            Business::MOBILE  => ['nullable','digits:11'],
            Business::PHONE   => ['nullable','numeric'],
            Business::EMAIL   => ['nullable','email', Rule::unique('businesses')->ignore($this->route('business'))]
        ];
    }
}
