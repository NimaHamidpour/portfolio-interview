<?php

namespace App\Http\Requests;

use App\Enum\User;
use App\Traits\ConvertDateTrait;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Morilog\Jalali\Jalalian;

class BaseRequest extends FormRequest
{
    use ConvertDateTrait;

    /**
     * @var array $dates Dates.
     */
    protected array $dates = [
        'birthdate',
        'from_birthdate',
        'to_birthdate',
        'created_at',
        'from_created_at',
        'to_created_at',
        'updated_at',
        'from_updated_at',
        'to_updated_at',
        'from_verified_at',
        'to_verified_at',
        'from_rejected_at',
        'to_rejected_at',
    ];

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [];
    }

    /**
     * failed validation
     *
     * @param Validator $validator Validator.
     *
     * @return void
     * @throws HttpResponseException Http Response Exception.
     */
    protected function failedValidation(Validator $validator)
    {
        $result = collect($validator->errors())->mapToGroups(function ($messageBag, $key) {
            return [
                [
                    'fieldName' => $key,
                    'message'   => $messageBag[0]
                ]
            ];
        });

        throw new HttpResponseException(response()->json([
            'errors' => [
                'validations' => $result->first()
            ]
        ], Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Convert jalali date to gregorian date request
     * @return void
     */
    protected function prepareForValidation(): void
    {
        foreach (array_filter($this->only($this->dates)) as $key => $value) {
            if ($this->validateJalaliDate($value)) {
                $this->merge([
                    $key => $this->gregorianDate(Jalalian::fromFormat('Y-m-d', $value))
                ]);
            }
        }
    }
}
