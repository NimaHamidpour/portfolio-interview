<?php

namespace App\Http\Requests;

use App\Enum\Config;
use Illuminate\Validation\Rule;

class StoreConfigRequest extends BaseRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            Config::KEY => ['required','string'],
            Config::VALUE => ['required','string'],
            Config::GROUP => ['nullable','string',Rule::in(Config::$configGroups)]
        ];
    }
}
