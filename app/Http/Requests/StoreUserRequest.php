<?php

namespace App\Http\Requests;

use App\Enum\User;
use App\Rules\IranNationalCode;

class StoreUserRequest extends BaseRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            User::USERNAME => ['nullable','string','unique:users,username'],
            User::FIRST_NAME => ['nullable','string'],
            User::LAST_NAME =>  ['nullable','string'],
            User::MOBILE => ['required','regex:/(09)[0-9]{9}/','digits:11','unique:users,mobile'],
            User::EMAIL => ['nullable','string','email','unique:users,email'],
            User::NATIONAL_CODE =>  ['nullable','unique:users,national_code', new IranNationalCode()],
            User::PHONE => ['nullable','numeric'],
            User::GENDER =>  ['nullable','string'],
            User::PROVINCE =>  ['nullable','string'],
            User::ADDRESS =>  ['nullable','string'],
            User::ZIPCODE => ['nullable','digits:10'],
            User::BIRTH_DATE => ['nullable','date_format:Y-m-d','before:today'],
            User::PASSWORD =>  ['nullable','string'],
        ];
    }
}
