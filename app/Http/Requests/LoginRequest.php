<?php

namespace App\Http\Requests;

use App\Enum\User;

class LoginRequest extends BaseRequest
{
    /**
     * @return boolean
     */
    public function authorize(): bool
    {
        $this->merge([User::REGISTER_IP => $this->ip()]);
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $validationRules = [
            User::MOBILE => 'required|regex:/(09)[0-9]{9}/|digits:11|numeric',
        ];

        if (str_contains($this->route()->getName(), 'login')) {
            $validationRules = array_merge($validationRules, [User::PASSWORD => 'required']);
        }

        if (str_contains($this->route()->getName(), 'verify')) {
            $validationRules = array_merge($validationRules, [User::OTP => 'required']);
        }

        return $validationRules;
    }
}
