<?php

namespace App\Http\Requests;

use App\Enum\User;
use App\Rules\IranNationalCode;
use Illuminate\Validation\Rule;
use Morilog\Jalali\Jalalian;

class UpdateUserRequest extends BaseRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            User::USERNAME => ['nullable','string',Rule::unique('users')->ignore($this->route('user'))],
            User::FIRST_NAME => ['nullable','string'],
            User::LAST_NAME =>  ['nullable','string'],
            User::EMAIL => ['nullable','string','email',Rule::unique('users')->ignore($this->route('user'))],
            User::NATIONAL_CODE =>  ['nullable',new IranNationalCode(),Rule::unique('users')->ignore($this->route('user'))],
            User::MOBILE => ['required','regex:/(09)[0-9]{9}/','digits:11',Rule::unique('users')->ignore($this->route('user'))],
            User::PHONE => ['nullable','numeric'],
            User::GENDER => ['nullable','numeric'],
            User::PROVINCE => ['nullable','numeric'],
            User::ADDRESS => ['nullable','string'],
            User::ZIPCODE => ['nullable','digits:10'],
            User::BIRTH_DATE => ['nullable','date_format:Y-m-d','before:yesterday'],
            User::PASSWORD =>  ['nullable','string'],
        ];
    }
}
