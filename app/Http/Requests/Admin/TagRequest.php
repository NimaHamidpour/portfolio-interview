<?php

namespace App\Http\Requests\Admin;

use App\Enum\Tag;
use App\Http\Requests\BaseRequest;

class TagRequest extends BaseRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            Tag::NAME => ['required', 'min:2', 'max:250'],
            Tag::COLOR => ['nullable', 'min:2', 'max:20'],
            Tag::IS_ACTIVE => ['required', 'boolean'],
        ];
    }
}
