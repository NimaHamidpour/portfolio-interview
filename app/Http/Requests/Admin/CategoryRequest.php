<?php

namespace App\Http\Requests\Admin;

use App\Enum\Category;
use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class CategoryRequest extends BaseRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            Category::SLUG => ['required', 'min:3', 'max:50'],
            Category::NAME => ['required', 'min:3', 'max:50'],
            Category::IS_ACTIVE => ['required', 'boolean'],
            Category::PRIORITY => ['nullable', 'numeric', 'between:0,10000'],
            Category::PARENT => ['nullable',
                Rule::exists(Category::TABLE, Category::CATEGORY_ID)
                    ->where(Category::DELETED_AT, null)],
        ];
    }
}
