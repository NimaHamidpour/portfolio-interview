<?php

namespace App\Http\Requests;

use App\Enum\User;

class PartialUpdateUserRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return boolean
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $validationRules = [];

        if ($this->has(User::PASSWORD)) {
            $validationRules = array_merge($validationRules, [
                User::PASSWORD => 'required|confirmed|min:6|regex:/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[#?!@$%^&*-]).{6,}$/',
            ]);
        }

        return array_merge($validationRules, [
            User::FIRST_NAME => 'required|string',
            User::LAST_NAME  => 'required|string',
        ]);

    }
}
