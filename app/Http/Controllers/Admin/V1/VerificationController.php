<?php

namespace App\Http\Controllers\Admin\V1;

use App\Enum\Verification as VerificationEnum;
use App\Filters\VerificationFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RejectVerificationRequest;
use App\Http\Requests\BaseRequest;
use App\Http\Resources\Admin\VerificationResource;
use App\Models\Verification;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class VerificationController extends Controller
{
    /**
     * @param VerificationFilter $filters VerificationFilter.
     * @param BaseRequest        $request BaseRequest.
     * @return AnonymousResourceCollection
     */
    public function index(VerificationFilter $filters, BaseRequest $request): AnonymousResourceCollection
    {
        $verifications = Verification::with([
                'user:user_id,first_name,last_name',
                'verifiedAdmin:user_id,first_name,last_name',
                'rejectedAdmin:user_id,first_name,last_name'
            ])
            ->filter($filters)
            ->paginate($this->getPageSize($request));

        return VerificationResource::collection($verifications);
    }

    /**
     * @param Verification $verification Verification.
     * @return VerificationResource
     */
    public function acceptVerification(Verification $verification): VerificationResource
    {
        $this->checkValidForUpdate($verification);
        $verification->status = VerificationEnum::ACCEPTED;
        $verification->verified_by = Auth::user()->user_id;
        $verification->verified_at = Carbon::now();
        $verification->save();
        return new VerificationResource($verification);
    }

    /**
     * @param Verification              $verification Verification.
     * @param RejectVerificationRequest $request      Request.
     * @return VerificationResource
     */
    public function rejectVerification(Verification $verification, RejectVerificationRequest $request): VerificationResource
    {
        $this->checkValidForUpdate($verification);
        $verification->status = VerificationEnum::REJECTED;
        $verification->rejected_by = Auth::user()->user_id;
        $verification->rejected_at = Carbon::now();
        $verification->reject_reason = $request->reject_reason;
        $verification->save();
        return new VerificationResource($verification);
    }

    /**
     * @param Verification $verification Verification.
     * @return void
     */
    private function checkValidForUpdate(Verification $verification): void
    {
        if ($verification->status != VerificationEnum::PENDING) {
            abort(Response::HTTP_BAD_REQUEST, __('messages.verification.verification_status_is_not_valid'));
        }
    }
}
