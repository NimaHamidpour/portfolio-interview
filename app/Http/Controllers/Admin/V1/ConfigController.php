<?php

namespace App\Http\Controllers\Admin\V1;

use App\Filters\ConfigFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\BaseRequest;
use App\Http\Requests\StoreConfigRequest;
use App\Http\Requests\UpdateConfigRequest;
use App\Http\Resources\Admin\ConfigResource;
use App\Interfaces\Models\ConfigInterface;
use App\Models\Config;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ConfigController extends Controller
{
    /**
     * @param ConfigFilter $filters Filter.
     * @param BaseRequest  $request Request.
     * @return AnonymousResourceCollection
     */
    public function index(ConfigFilter $filters, BaseRequest $request): AnonymousResourceCollection
    {
        return ConfigResource::collection(Config::filter($filters)->paginate($this->getPageSize($request)));
    }

    /**
     * @param StoreConfigRequest $request Request.
     * @return ConfigInterface
     */
    public function store(StoreConfigRequest $request)
    {
        return Config::createObject($request->key, $request->value, $request->group);
    }

    /**
     * @param Config              $config  Config.
     * @param UpdateConfigRequest $request Request.
     * @return ConfigInterface
     */
    public function update(Config $config, UpdateConfigRequest $request)
    {
        return $config->updateObject($request->key, $request->value, $request->group);
    }

    /**
     * @param Config $config Config.
     * @return boolean
     */
    public function destroy(Config $config)
    {
        return $config->delete();
    }
}
