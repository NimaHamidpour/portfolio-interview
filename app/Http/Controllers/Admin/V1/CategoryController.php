<?php

namespace App\Http\Controllers\Admin\V1;

use App\Exceptions\OdinException;
use App\Filters\CategoryFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CategoryRequest;
use App\Http\Requests\BaseRequest;
use App\Http\Resources\Admin\CategoryResource;
use App\Lib\UserVerification\Verifications\PhoneVerification;
use App\Models\Category;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param CategoryFilter $filters CategoryFilter.
     * @param BaseRequest    $request Request.
     * @return AnonymousResourceCollection
     */
    public function index(CategoryFilter $filters, BaseRequest $request): AnonymousResourceCollection
    {
        return CategoryResource::collection(Category::filter($filters)
            ->with('ancestors')->paginate($this->getPageSize($request)));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategoryRequest $request CategoryRequest.
     * @return CategoryResource
     */
    public function store(CategoryRequest $request)
    {
        $parentCategory = empty($request->parent) ? null : Category::findOrFail($request->parent);

        try {
            DB::beginTransaction();
            $category = Category::create($request->all());

            if (!is_null($parentCategory)) {
                $category->appendToNode($parentCategory)->save();
            }
            DB::commit();
            return new CategoryResource($category);
        } catch (\Exception $e) {
            DB::rollBack();
            abort(Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category Category.
     * @return CategoryResource
     */
    public function show(Category $category): CategoryResource
    {
        return new CategoryResource($category->load('ancestors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Category        $category Category.
     * @param CategoryRequest $request  CategoryRequest.
     * @return CategoryResource
     */
    public function update(Category $category, CategoryRequest $request): CategoryResource
    {
        $parentCategory = empty($request->parent) ? null : Category::findOrFail($request->parent);

        try {
            DB::beginTransaction();
            $category->update($request->all());

            if (!is_null($parentCategory)) {
                $category->appendToNode($parentCategory)->save();
            }

            DB::commit();
            return new CategoryResource($category);
        } catch (\Exception $e) {
            DB::rollBack();
            abort(Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category Category.
     * @return JsonResponse
     */
    public function destroy(Category $category): JsonResponse
    {
        $category->delete();
        return $this->sendSuccess();
    }
}
