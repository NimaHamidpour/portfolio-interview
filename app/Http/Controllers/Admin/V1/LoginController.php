<?php

namespace App\Http\Controllers\Admin\V1;

use App\Filters\LoginFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\BaseRequest;
use App\Http\Resources\Admin\LoginResource;
use App\Models\Login;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class LoginController extends Controller
{
    /**
     * @param LoginFilter $filters Login Filters.
     * @param BaseRequest $request Request.
     *
     * @return AnonymousResourceCollection
     */
    public function index(LoginFilter $filters, BaseRequest $request): AnonymousResourceCollection
    {
        return LoginResource::collection(Login::filter($filters)->paginate($this->getPageSize($request)));
    }
}
