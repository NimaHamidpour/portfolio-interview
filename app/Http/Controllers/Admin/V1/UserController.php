<?php

namespace App\Http\Controllers\Admin\V1;

use App\Filters\UserFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\BaseRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\Admin\UserResource;
use App\Interfaces\Models\UserInterface;
use App\Models\User;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class UserController extends Controller
{
    /**
     * @param UserFilter  $filters User Filters.
     * @param BaseRequest $request Request.
     *
     * @return AnonymousResourceCollection
     */
    public function index(UserFilter $filters, BaseRequest $request): AnonymousResourceCollection
    {
        return UserResource::collection(User::filter($filters)->paginate($this->getPageSize($request)));
    }

    /**
     * @param User $user User.
     *
     * @return UserResource
     */
    public function show(User $user): UserResource
    {
        return new UserResource($user);
    }

    /**
     * @param StoreUserRequest $request Request.
     *
     * @return UserInterface
     */
    public function store(StoreUserRequest $request): UserInterface
    {
        return User::createObject(
            $request->mobile,
            $request->type,
            $request->username,
            $request->first_name,
            $request->last_name,
            $request->national_code,
            $request->email,
            $request->phone,
            $request->address,
            $request->zipcode,
            $request->birthdate,
            $request->fcm_token,
            $request->register_ip,
            $request->status,
            $request->allow_charge,
            $request->is_synced_crm,
            $request->check_ip,
            $request->lock_balance,
            $request->lock_withdraw,
            $request->password,
        );
    }

    /**
     * @param User              $user    User.
     * @param UpdateUserRequest $request Request.
     *
     * @return UserInterface
     */
    public function update(User $user, UpdateUserRequest $request): UserInterface
    {
        return $user->updateObject(
            $request->mobile,
            $request->type,
            $request->username,
            $request->first_name,
            $request->last_name,
            $request->national_code,
            $request->email,
            $request->phone,
            $request->address,
            $request->zipcode,
            $request->birthdate,
            $request->fcm_token,
            $request->register_ip,
            $request->status,
            $request->allow_charge,
            $request->is_synced_crm,
            $request->check_ip,
            $request->lock_balance,
            $request->lock_withdraw,
            $request->password,
        );
    }

    /**
     * @param User $user User.
     *
     * @return boolean
     */
    public function destroy(User $user)
    {
        return $user->delete();
    }
}
