<?php

namespace App\Http\Controllers\Admin\V1;

use App\Filters\TagFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TagRequest;
use App\Http\Requests\BaseRequest;
use App\Http\Resources\Admin\TagResource;
use App\Models\Tag;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param TagFilter   $filters TagFilter.
     * @param BaseRequest $request Request.
     * @return AnonymousResourceCollection
     */
    public function index(TagFilter $filters, BaseRequest $request): AnonymousResourceCollection
    {
        return TagResource::collection(Tag::filter($filters)
            ->paginate($this->getPageSize($request)));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TagRequest $request TagRequest.
     * @return TagResource
     */
    public function store(TagRequest $request): TagResource
    {
        $tag = Tag::findOrCreate($request->name);
        $tag->is_active = $request->is_active;
        $tag->color = $request->color;
        $tag->save();

        return new TagResource($tag);
    }

    /**
     * Display the specified resource.
     *
     * @param Tag $tag Tag.
     * @return TagResource
     */
    public function show(Tag $tag): TagResource
    {
        return new TagResource($tag);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Tag        $tag     Tag.
     * @param TagRequest $request TagRequest.
     * @return TagResource
     */
    public function update(Tag $tag, TagRequest $request): TagResource
    {
        $tag->update($request->all());
        return new TagResource($tag);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Tag $tag Tag.
     * @return JsonResponse
     */
    public function destroy(Tag $tag): JsonResponse
    {
        $tag->delete();
        return $this->sendSuccess();
    }
}
