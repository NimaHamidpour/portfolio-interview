<?php

namespace App\Http\Controllers\Common\V1;

use App\Enum\Verification as VerificationEnum;
use App\Http\Controllers\Controller;
use App\Models\Verification;
use Carbon\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;

class VerificationController extends Controller
{
    /**
     * @var string
     */
    private string $rejectionMessage;

    /**
     * @param  Request $request Request.
     * @return Application|RedirectResponse|Redirector|void
     */
    public function verifyEmailVerification(Request $request)
    {
        abort_if(empty($token = $request->get('code')), 404);

        try {
            $token = json_decode(decrypt($token));

            if (empty($token->verificationId) || empty($token->expirationTimestamp)) {
                abort(Response::HTTP_BAD_REQUEST, __('messages.verification.token_is_not_valid'));
            }

            if (empty($emailVerification = Verification::find($token->verificationId))) {
                abort(Response::HTTP_BAD_REQUEST, __('messages.verification.token_verification_not_found'));
            }

            if ($this->isTokenExpired($emailVerification, $token)) {
                $deeplink = sprintf(
                    '%s?status=rejected',
                    config('services.verification.email_verification_deeplink'),
                    $this->rejectionMessage
                );
                return redirect($deeplink);
            }

            $emailVerification->status = VerificationEnum::ACCEPTED;
            $emailVerification->verified_at = Carbon::now();
            $emailVerification->save();
            $deeplink = sprintf(
                '%s?status=accepted',
                config('services.verification.email_verification_deeplink')
            );
            return redirect($deeplink);
        } catch (DecryptException $e) {
            abort(Response::HTTP_BAD_REQUEST, __('messages.verification.token_is_not_valid'));
        }
    }

    /**
     * @param Verification $emailVerification EmailVerification.
     * @param string       $token             Token.
     * @return boolean
     */
    private function isTokenExpired(Verification $emailVerification, string $token): bool
    {
        if (Carbon::now()->timestamp > $token->expirationTimestamp) {
            $emailVerification->status = VerificationEnum::REJECTED;
            $emailVerification->rejected_at = Carbon::now();
            $this->rejectionMessage = __('messages.verification.token_is_expired');
            $emailVerification->reject_reason = $this->rejectionMessage;
            $emailVerification->save();
            return true;
        }

        return false;
    }
}
