<?php

namespace App\Http\Controllers\Common\V1;

use App\Enum\User as EnumUser;
use App\Events\UserLoggedInEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Resources\Public\OtpResource;
use App\Http\Resources\Public\TokenResource;
use App\Models\User;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Lib\Otp\Otp;
use App\Lib\Otp\RedisDriver;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    /**
     * @param LoginRequest $request Login Request.
     * @param Otp          $otp     One Time Password.
     * @return OtpResource
     *
     * @throws Exception \Throwable.
     */
    public function loginOrRegisterWithOtp(LoginRequest $request, Otp $otp): OtpResource
    {
        $mobile = $request->get(EnumUser::MOBILE);

        $user = User::firstOrCreate([
            EnumUser::MOBILE => $mobile,
        ], [
            EnumUser::REGISTER_IP => $request->get(EnumUser::REGISTER_IP),
        ]);

        $otp->setDriver(new RedisDriver())->setOtp($mobile);
        // TODO: dispatch notification (sms or ...) with this $otp for user

        return (new OtpResource($user))->setMessage(__('auth.otp_send_success'));
    }

    /**
     * @param LoginRequest $request Login Request.
     * @param Otp          $otp     Otp.
     * @return TokenResource
     *
     */
    public function otp(LoginRequest $request, Otp $otp): TokenResource
    {
        $mobile = $request->get(EnumUser::MOBILE);
        $otp
            ->setDriver(new RedisDriver())
            ->checkRandomNumberForKey($mobile, $request->get(EnumUser::OTP));
        $user = User::whereMobile($mobile)->first();

        return (new TokenResource($user, JWTAuth::fromUser($user)))->setMessage(__('auth.otp_succeeded'));
    }

    /**
     * @param LoginRequest $request Login Request.
     * @return TokenResource
     * @throws Exception \Throwable.
     */
    public function login(LoginRequest $request): TokenResource
    {
        $mobile = $request->get(EnumUser::MOBILE);
        $user = User::whereMobile($mobile)->firstOrFail();
        abort_if(
            !(
                $user &&
                Hash::check(
                    $request->get(EnumUser::PASSWORD),
                    $user->password
                )
            ),
            Response::HTTP_FORBIDDEN,
            __('auth.mobile_or_password_incorrect')
        );
        event(new UserLoggedInEvent($user, $request->ip(), null));
        return (new TokenResource($user, JWTAuth::fromUser($user)))->setMessage(__('auth.login_succeeded'));
    }
}
