<?php

namespace App\Http\Controllers\Common\V1;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 *
 */
class DocumentationController extends Controller
{
    /**
     * @param string $documentType DocumentType.
     * @return Factory|View|Application
     */
    public function showAdminApiDocumentation(string $documentType = 'rapidoc'): Factory|View|Application
    {
        $this->validateDocumentType($documentType);
        $documentPath = asset('open-api3/admin/admin.yml');
        return view(sprintf('api-documentation.%s', $documentType), compact('documentPath'));
    }

    /**
     * @param string $documentType DocumentType.
     * @return Factory|View|Application
     */
    public function showClientApiDocumentation(string $documentType = 'rapidoc'): Factory|View|Application
    {
        $this->validateDocumentType($documentType);
        $documentPath = asset('open-api3/client/client.yml');
        return view(sprintf('api-documentation.%s', $documentType), compact('documentPath'));
    }

    /**
     * @param string $documentType DocumentType.
     * @return void
     * @throws NotFoundHttpException    NotFoundHttpException.
     */
    private function validateDocumentType(string $documentType): void
    {
        $validDocumentTypes = ['swagger-ui', 'redoc', 'rapidoc'];

        if (!in_array($documentType, $validDocumentTypes)) {
            abort(Response::HTTP_NOT_FOUND);
        }
    }
}
