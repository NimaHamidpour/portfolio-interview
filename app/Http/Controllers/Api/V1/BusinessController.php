<?php

namespace App\Http\Controllers\Api\V1;

use App\Enum\User as EnumUser;
use App\Filters\BusinessFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\BaseRequest;
use App\Http\Requests\PartialUpdateUserRequest;
use App\Http\Requests\StoreBusinessRequest;
use App\Http\Requests\UpdateBusinessRequest;
use App\Http\Resources\Client\BusinessResource;
use App\Http\Resources\Client\UserResource;
use App\Models\Business;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class BusinessController extends Controller
{
    /**
     * @param BusinessFilter $filters Filters.
     * @param BaseRequest    $request Request.
     * @return AnonymousResourceCollection
     */
    public function index(BusinessFilter $filters, BaseRequest $request): AnonymousResourceCollection
    {
        return BusinessResource::collection(Business::filter($filters)
            ->with('user', 'category')->paginate($this->getPageSize($request)));
    }
    /**
     * @param StoreBusinessRequest $request Request.
     * @return BusinessResource
     */
    public function store(StoreBusinessRequest $request): BusinessResource
    {
        $business = Business::create($request->all());
        return new BusinessResource($business);
    }

    /**
     * @param Business $business Business.
     * @return BusinessResource
     */
    public function show(Business $business): BusinessResource
    {
        return new BusinessResource($business);
    }
    /**
     * @param Business              $business Business.
     * @param UpdateBusinessRequest $request  Request.
     * @return BusinessResource
     */
    public function update(Business $business, UpdateBusinessRequest $request)
    {
        $business->update($request->all());
        return new BusinessResource($business);
    }
    /**
     * @param Business $business Business.
     * @return JsonResponse
     */
    public function destroy(Business $business): JsonResponse
    {
        $business->delete();
        return $this->sendSuccess();
    }
}
