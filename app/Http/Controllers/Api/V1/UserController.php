<?php

namespace App\Http\Controllers\Api\V1;

use App\Enum\User as EnumUser;
use App\Http\Controllers\Controller;
use App\Http\Requests\PartialUpdateUserRequest;
use App\Http\Resources\Client\UserResource;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * @param PartialUpdateUserRequest $request Partial Update User Request.
     * @return UserResource
     */
    public function update(PartialUpdateUserRequest $request): UserResource
    {
        $user = User::whereUserId(Auth::id())->firstOrFail();

        if (!empty($request->password)) {
            $request->merge([EnumUser::PASSWORD => Hash::make($request->password)]);
        }
        $user->update($request->all());

        return (new UserResource($user))->setMessage(__('message.updated'));
    }
}
