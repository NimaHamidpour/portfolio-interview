<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\Client\SelfResource;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class SelfController extends Controller
{
    /**
     * @return SelfResource
     */
    public function userinfo(): SelfResource
    {
        $user = User::whereUserId(Auth::id())->firstOrFail();
        return new SelfResource($user);
    }
}
