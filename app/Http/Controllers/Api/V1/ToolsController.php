<?php

namespace App\Http\Controllers\Api\V1;

use App\Enum\Config as EnumConfig;
use App\Filters\ConfigFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\BaseRequest;
use App\Http\Resources\Client\ConfigResource;
use App\Models\Config;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ToolsController extends Controller
{
    /**
     * @param ConfigFilter $filters Filter.
     * @param BaseRequest  $request Request.
     * @return AnonymousResourceCollection
     */
    public function getConfigs(ConfigFilter $filters, BaseRequest $request): AnonymousResourceCollection
    {
        $query = Config::filter($filters);

        if (!empty($request->key)) {
            abort_if(!in_array($request->key, EnumConfig::$clientConfig), Response::HTTP_FORBIDDEN);
        }

        if (!empty($request->group)) {
            abort_if(!in_array($request->group, EnumConfig::$clientConfig), Response::HTTP_FORBIDDEN);
        }

        return ConfigResource::collection(
            $query->whereIn(
                EnumConfig::KEY,
                EnumConfig::$clientConfig
            )->orWhereIn(
                EnumConfig::GROUP,
                EnumConfig::$clientConfig
            )->paginate($this->getPageSize($request))
        );
    }
}
