<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\VerificationRequest;
use App\Http\Resources\Client\VerificationResource;
use App\Lib\UserVerification\VerificationFactory;
use Illuminate\Support\Facades\Auth;

class VerificationController extends Controller
{
    /**
     * @param VerificationRequest $request Request.
     * @return mixed
     */
    public function store(VerificationRequest $request): VerificationResource
    {
        $verification = VerificationFactory::makeVerification($request->type, Auth::user());
        return new VerificationResource($verification->addVerification($request->post('data')));
    }
}
