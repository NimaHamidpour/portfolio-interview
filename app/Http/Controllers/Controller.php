<?php

namespace App\Http\Controllers;

use App\Traits\ResponseTrait;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use ResponseTrait;
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    const DEFAULT_PAGE_SIZE = 10;
    const PER_PAGE = 'per_page';

    /**
     * @param Request $request Set Pagination Size.
     *
     * @return integer
     */
    protected function getPageSize(Request $request): int
    {
        $pageSize = self::DEFAULT_PAGE_SIZE;
        if ($request->has(self::PER_PAGE) && !empty($request->get(self::PER_PAGE))) {
            $pageSize = (int)$request->get(self::PER_PAGE);
        }
        return $pageSize;
    }
}
