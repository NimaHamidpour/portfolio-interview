<?php

namespace App\Http\Resources;

use App\Traits\ConvertDateTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BaseResource extends JsonResource
{
    use ConvertDateTrait;

    /**
     * @var string
     */
    private string $message = '';

    /**
     * @param string $message Message.
     *
     * @return BaseResource
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param Request $request
     * @return string[]
     */
    public function with($request): array
    {
        return [
            'message' => $this->getMessage(),
        ];
    }

    /**
     * @return array
     */
    public function getDatesAsJalaliDateTime(): array
    {
        return [
            'createdAt' => !empty($this->created_at) ? $this->jalaliDateTime($this->created_at) : null,
            'updatedAt' => !empty($this->updated_at) ? $this->jalaliDateTime($this->updated_at) : null,
        ];
    }
}
