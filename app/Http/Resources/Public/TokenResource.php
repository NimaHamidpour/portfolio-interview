<?php

namespace App\Http\Resources\Public;

use App\Http\Resources\BaseResource;
use Illuminate\Http\Request;

class TokenResource extends BaseResource
{
    /**
     * @var string
     */
    public string $token;

    /**
     * TokenResource constructor.
     * @param $resource
     * @param string $token
     */
    public function __construct($resource, string $token)
    {
        parent::__construct($resource);
        $this->token = $token;
    }

    /**
     * Transform the resource into an array.
     *
     * @param Request $request Request.
     * @return array
     */
    public function toArray($request)
    {
        return [
            'token' => $this->token,
        ];
    }
}
