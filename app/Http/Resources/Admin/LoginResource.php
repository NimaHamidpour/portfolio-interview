<?php

namespace App\Http\Resources\Admin;

use App\Http\Resources\BaseResource;
use Illuminate\Http\Request;

class LoginResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request BaseRequest.
     *
     * @return array
     */
    public function toArray($request)
    {
        return array_merge([
            'id' => $this->login_id,
            'ip' => $this->ip,
            'platform' => $this->platform,
            'user' => UserResource::collection($this->whenLoaded('user')),
        ], $this->getDatesAsJalaliDateTime());
    }
}
