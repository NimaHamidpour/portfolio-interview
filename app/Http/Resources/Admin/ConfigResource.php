<?php

namespace App\Http\Resources\Admin;

use App\Http\Resources\BaseResource;
use Illuminate\Http\Request;

class ConfigResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request BaseRequest.
     *
     * @return array
     */
    public function toArray($request)
    {
        return array_merge([
            'id' => $this->config_id,
            'key' => $this->key,
            'value' => $this->value,
            'group' => $this->group,
        ], $this->getDatesAsJalaliDateTime());
    }
}
