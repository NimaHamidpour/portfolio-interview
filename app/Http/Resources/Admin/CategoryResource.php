<?php

namespace App\Http\Resources\Admin;

use App\Http\Resources\BaseResource;
use Illuminate\Http\Request;

class CategoryResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request BaseRequest.
     *
     * @return array
     */
    public function toArray($request): array
    {
        return array_merge([
            'id' => $this->category_id,
            'slug' => $this->slug,
            'name' => $this->name,
            'priority' => $this->priority,
            'isActive' => $this->is_active,
            'logo' => '',
            'ancestors' => $this->collection($this->whenLoaded('ancestors')),
        ], $this->getDatesAsJalaliDateTime());
    }
}
