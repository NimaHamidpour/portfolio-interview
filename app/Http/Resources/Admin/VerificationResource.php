<?php

namespace App\Http\Resources\Admin;

use App\Http\Resources\BaseResource;

class VerificationResource extends BaseResource
{
    public function toArray($request)
    {
        return array_merge([
            'id' => $this->verification_id,
            'user' => [
                'id' => $this->user->user_id,
                'name' => $this->user->full_name,
            ],
            'type' => [
                'value' => $this->verification_type,
                'translation' =>
                    __(sprintf('%s.%s', 'messages.verification', $this->verification_type)),
            ],
            'status' => [
                'value' => $this->status,
                'translation' =>
                    __(sprintf('%s.%s', 'messages.verification', strtolower($this->status))),
            ],
            'value' => empty($this->value) ? null : json_decode($this->value),
            'clientDescription' => $this->user_id,
            'verifiedInfo' => empty($this->verified_at) ? null : [
                'verifiedAdminId' => $this->verifiedAdmin->user_id,
                'verifiedAdminName' => $this->verifiedAdmin->full_name,
                'verifiedAt' => $this->jalaliDateTime($this->verified_at),
            ],
            'rejectedInfo' => empty($this->rejected_at) ? null : [
                'rejectedAdminId' => $this->rejectedAdmin->user_id,
                'rejectedAdminName' => $this->rejectedAdmin->full_name,
                'rejectReason' => $this->reject_reason,
                'rejectedAt' => $this->jalaliDateTime($this->rejected_at),
            ],
        ], $this->getDatesAsJalaliDateTime());
    }
}
