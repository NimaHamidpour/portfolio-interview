<?php

namespace App\Http\Resources\Admin;

use App\Http\Resources\BaseResource;
use Illuminate\Support\Carbon;

class UserResource extends BaseResource
{
    /**
     * Transform the resource into an array
     *
     * @return array
     */
    public function toArray($request)
    {
        return array_merge([
            'id' => $this->user_id,
            'username' => $this->username,
            'firstName' => $this->first_name,
            'lastName' => $this->last_name,
            'nationalCode'=> $this->national_code,
            'mobile' => $this->mobile,
            'email' => $this->email,
            'phone' => $this->phone,
            'zipcode' => $this->zipcode,
            'address' => $this->address,
            'birthdate' => $this->birthdate ? $this->jalaliDate(Carbon::parse($this->birthdate)) : null,
            'registerIp' => $this->register_ip,
            'fcmToken' => $this->fcm_token,
            'status' => $this->status,
            'isSyncedCRM' => $this->is_synced_crm,
            'checkIp' => $this->check_ip,
            'type' => $this->type,
            'allowCharge' => $this->allow_charge,
            'lockBalance' => $this->lock_balance,
            'lockWithDraw'=> $this->lock_withdraw,
            'deletedAt' => $this->deleted_at ?  $this->jalaliDateTime($this->deleted_at) : null
        ], $this->getDatesAsJalaliDateTime());
    }
}
