<?php

namespace App\Http\Resources\Client;

use App\Http\Resources\BaseResource;
use Illuminate\Support\Carbon;

class LuckResource extends BaseResource
{

    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->luck_id,
            'business' => $this->whenLoaded($this->business),
            'title' => $this->title
        ];
    }
}
