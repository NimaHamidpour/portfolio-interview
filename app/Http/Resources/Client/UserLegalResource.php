<?php

namespace App\Http\Resources\Client;

use Illuminate\Http\Request;
use App\Http\Resources\BaseResource;

class UserLegalResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request BaseRequest.
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
                'id' => $this->legal_info_id,
                'companyName' => $this->company_name,
                'companyRegistrationNumber' => $this->company_registration_number,
                'companyNationalCode' => $this->company_national_code,
                'companyEconomicCode' => $this->company_economic_code,
                'companyTaxCode' => $this->company_tax_code
                ];
    }
}
