<?php

namespace App\Http\Resources\Client;

use App\Http\Resources\BaseResource;
use Illuminate\Support\Carbon;

class ProductResource extends BaseResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'            => $this->product_id,
            'sku'           => $this->sku,
            'business'      => $this->whenLoaded($this->business),
            'category'      => $this->whenLoaded($this->category),
            'brand'         => $this->brand,
            'quantity'      => $this->quantity,
            'price'         => $this->price,
            'discountPrice' => $this->discount_price,
            'description'   => $this->description,
            'detail'        => $this->detail,
            'color'         => $this->color,
            'deletedAt'     => $this->deleted_at ?  $this->jalaliDateTime($this->deleted_at) : null
        ];
    }
}
