<?php

namespace App\Http\Resources\Client;

use App\Http\Resources\BaseResource;
use Illuminate\Http\Request;

class ConfigResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request BaseRequest.
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->config_id,
            'key' => $this->key,
            'value' => $this->value,
            'group' => $this->group,
        ];
    }
}
