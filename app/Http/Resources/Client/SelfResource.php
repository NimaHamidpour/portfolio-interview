<?php

namespace App\Http\Resources\Client;

use App\Enum\User;
use App\Http\Resources\BaseResource;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;


class SelfResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request BaseRequest.
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'userInfo' => [
                User::USER_ID           => $this->user_id,
                User::USERNAME          => $this->username,
                User::FIRST_NAME        => $this->first_name,
                User::LAST_NAME         => $this->last_name,
                User::NATIONAL_CODE     => $this->national_code,
                User::MOBILE            => $this->mobile,
                User::EMAIL             => $this->email,
                User::PHONE             => $this->phone,
                User::ZIPCODE           => $this->zipcode,
                User::ADDRESS           => $this->address,
                User::BIRTH_DATE        => $this->birthdate ? $this->jalaliDate(Carbon::parse($this->birthdate)) : null,
                User::REGISTER_IP       => $this->register_ip,
                User::STATUS            => $this->status,
                User::ALLOW_CHARGE      => $this->allow_charge,
                User::LOCK_BALANCE      => $this->lock_balance,
                User::LOCK_WITHDRAW     => $this->lock_withdraw,
                User::CREATED_AT        => $this->created_at ?  $this->jalaliDateTime($this->created_at) : null,
                User::UPDATED_AT        => $this->updated_at ?  $this->jalaliDateTime($this->updated_at) : null,
                User::DELETED_AT        => $this->deleted_at ?  $this->jalaliDateTime($this->deleted_at) : null
            ],

            'userBanksInfo' => UserBankResource::collection($this->bankInfo),
        ];
    }
}
