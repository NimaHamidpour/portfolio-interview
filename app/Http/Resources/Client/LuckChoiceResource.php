<?php

namespace App\Http\Resources\Client;

use App\Http\Resources\BaseResource;
use Illuminate\Support\Carbon;

class LuckChoiceResource extends BaseResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'    => $this->luck_choice_id,
            'luck'  => $this->whenLoaded($this->luck),
            'type'  => $this->type,
            'value' => $this->value,
        ];
    }
}
