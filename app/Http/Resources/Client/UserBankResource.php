<?php

namespace App\Http\Resources\Client;

use Illuminate\Http\Request;
use App\Http\Resources\BaseResource;

class UserBankResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request BaseRequest.
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->user_bank_id,
            'bank' =>$this->bank_name,
            'priority' => $this->owner_name,
            'cardNumber' => $this->card_number,
            'sheba' => $this->sheba,
        ];
    }
}
