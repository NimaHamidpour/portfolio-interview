<?php

namespace App\Http\Resources\Client;

use App\Http\Resources\BaseResource;
use Illuminate\Support\Carbon;

class BusinessResource extends BaseResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'          => $this->business_id,
            'user'        => $this->user,
            'category'    => $this->category,
            'city'        => $this->city,
            'title'       => $this->title,
            'description' => $this->description,
            'region'      => $this->region,
            'map'         => $this->map,
            'mobile'      => $this->mobile,
            'phone'       => $this->phone,
            'website'     => $this->website,
            'whatsapp'    => $this->whatsapp,
            'telegram'    => $this->telegram,
            'instagram'   => $this->instagram,
            'email'       => $this->email,
            'wallet'      => $this->wallet,
            'lockBalance' => $this->lock_balance,
            'lockWithDraw'=> $this->lock_withdraw,
            'deletedAt'   => $this->deleted_at ?  $this->jalaliDateTime($this->deleted_at) : null
        ];
    }
}
