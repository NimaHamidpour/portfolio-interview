<?php

namespace App\Http\Middleware;

use App\Enum\Role;
use Closure;
use Spatie\Permission\Middlewares\PermissionMiddleware;
use Symfony\Component\HttpFoundation\Response;

class CheckPermissionMiddleware extends PermissionMiddleware
{
    public function handle($request, Closure $next, $permission = null, $guard = null)
    {
        $authGuard = app('auth')->guard($guard);

        if ($authGuard->guest()) {
            abort(Response::HTTP_FORBIDDEN);
        }

        if ($authGuard->user()->hasRole(Role::SUPER_ADMIN)) {
            return $next($request);
        }

        if (empty($permission)) {
            $routePermission = $request->route()->getName();
            $allRoutePermission = makeAllRouteName($routePermission);
            $permissions = [$allRoutePermission, $routePermission];
        } else {
            $permissions = is_array($permission) ? $permission : explode('|', $permission);
        }

        foreach ($permissions as $permission) {
            if ($authGuard->user()->can($permission)) {
                return $next($request);
            }
        }

        abort(Response::HTTP_FORBIDDEN);
    }
}
