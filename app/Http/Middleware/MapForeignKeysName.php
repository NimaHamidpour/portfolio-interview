<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class MapForeignKeysName
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        foreach ($request->all() as $key => $input) {
            if (str_ends_with($key, '_id')) {
                unset($request[$key]);
                $request->merge([sprintf('fk_%s', $key) => $input ]);
            }
        }

        return $next($request);
    }
}
