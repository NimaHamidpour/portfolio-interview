<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class BankCard implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (strlen($value) < 16 || intval($value.substr(1,10)) == 0 || intval($value.substr(10,6)) == 0){
            return false;
        }
        $c = intval(substr($value,15,1));
        $s = 0;
        for($i = 0; $i < 16 ; $i++)
        {
            $k = ($i % 2 == 0) ? 2 : 1;
            $d = intval(substr($value,$i,1)) * $k;
            $s += ($d > 9) ? $d - 9 : $d;
        }
        return (($s % 10) == 0);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'شماره کارت بانک نامعتبر می باشد';
    }
}
