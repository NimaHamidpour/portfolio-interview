<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IranNationalCode implements Rule
{
    /**
     * @param string $attribute Attribute.
     * @param mixed  $value     Value.
     * @return boolean
     */
    public function passes($attribute, mixed $value)
    {
        if (!preg_match('/^[0-9]{10}$/', $value)) {
            return false;
        }

        $sum = 0;
        for ($i = 0; $i < 9; $i++) {
            $sum += ((10 - $i) * intval(substr($value, $i, 1)));
        }
        $remainder = $sum % 11;
        $check = intval(substr($value, 9, 1));
        if (($remainder < 2 && $remainder == $check) || ($remainder >= 2 && $remainder == 11 - $check)) {
            return true;
        }
        return false;
    }


    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'فرمت کد ملی نامعتبر می باشد';
    }
}
