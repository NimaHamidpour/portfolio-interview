<?php

namespace App\Jobs;

use App\Models\Login;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class StoreUserLoginInfo implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * Create a new job instance.
     *
     * @param integer     $fkUserId FkUserId.
     * @param string      $ip       Ip.
     * @param string|null $platform Platform.
     */
    public function __construct(public int $fkUserId, public string $ip, public string|null $platform)
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Login::createObject($this->fkUserId, $this->ip, $this->platform);
    }
}
